SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `access_violations` (
`id` int(11) NOT NULL,
  `username` varchar(55) NOT NULL,
  `ip` text NOT NULL,
  `user_agent` varchar(55) NOT NULL,
  `time` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1622 ;

CREATE TABLE IF NOT EXISTS `api_keys` (
  `User_ID` int(11) NOT NULL,
`id` int(11) NOT NULL,
  `Public_Key` text NOT NULL,
  `Authentication_Key` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=362 ;

CREATE TABLE IF NOT EXISTS `balances` (
  `User_ID` int(11) NOT NULL,
  `Amount` text NOT NULL,
  `Coin` text NOT NULL,
`id` int(11) NOT NULL,
  `Pending` int(11) NOT NULL,
  `Wallet_ID` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27352 ;

CREATE TABLE IF NOT EXISTS `balance_history` (
`id` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Amount` text NOT NULL,
  `Timestamp` int(11) NOT NULL,
  `Wallet_ID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3122 ;

CREATE TABLE IF NOT EXISTS `bantables_ip` (
  `date` text NOT NULL,
  `ip` varchar(55) NOT NULL,
`id` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

CREATE TABLE IF NOT EXISTS `canceled_trades` (
`Id` int(11) NOT NULL,
  `To` text NOT NULL,
  `From` text NOT NULL,
  `Amount` text NOT NULL,
  `Value` double NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Type` text NOT NULL,
  `Finished` int(11) NOT NULL DEFAULT '0',
  `Fee` text NOT NULL,
  `Total` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4342 ;

CREATE TABLE IF NOT EXISTS `config` (
`id` int(2) NOT NULL,
  `name` varchar(20) NOT NULL,
  `setting` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=62 ;

CREATE TABLE IF NOT EXISTS `deposits` (
  `Transaction_Id` text NOT NULL,
  `Amount` text NOT NULL,
  `Coin` text NOT NULL,
  `Paid` int(11) NOT NULL,
`id` int(11) NOT NULL,
  `Account` text
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6492 ;

CREATE TABLE IF NOT EXISTS `messages` (
`id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `username` varchar(55) NOT NULL,
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `color` text NOT NULL,
  `timestamp` text NOT NULL,
  `hidden` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1622 ;

CREATE TABLE IF NOT EXISTS `ticketreplies` (
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` longtext NOT NULL,
  `posted` text NOT NULL,
`id` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=249 ;

CREATE TABLE IF NOT EXISTS `tickets` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` text NOT NULL,
  `posted` text NOT NULL,
  `opened` int(11) DEFAULT '1',
  `body` longblob NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=142 ;

CREATE TABLE IF NOT EXISTS `trades` (
`Id` int(11) NOT NULL,
  `To` text NOT NULL,
  `From` text NOT NULL,
  `Amount` text NOT NULL,
  `Value` double NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Type` text NOT NULL,
  `Finished` int(11) NOT NULL DEFAULT '0',
  `Fee` text NOT NULL,
  `Total` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4412 ;

CREATE TABLE IF NOT EXISTS `trade_history` (
  `Market_Id` text NOT NULL,
  `Price` text NOT NULL,
  `Quantity` text NOT NULL,
  `Timestamp` text NOT NULL,
`trade_id` int(11) NOT NULL,
  `Buyer` int(11) NOT NULL,
  `Seller` int(11) NOT NULL,
  `B_Receive` text,
  `S_Receive` text
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2242 ;

CREATE TABLE IF NOT EXISTS `transfers` (
`id` int(11) NOT NULL,
  `Sender_ID` int(11) NOT NULL,
  `Receiver_ID` int(11) NOT NULL,
  `amount` text NOT NULL,
  `Timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

CREATE TABLE IF NOT EXISTS `usercake_groups` (
`Group_ID` int(11) NOT NULL,
  `Group_Name` varchar(225) NOT NULL,
  `Color` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

CREATE TABLE IF NOT EXISTS `usercake_users` (
`User_ID` int(11) NOT NULL,
  `Username` varchar(150) NOT NULL,
  `Username_Clean` varchar(150) NOT NULL,
  `Password` varchar(225) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `ActivationToken` varchar(225) NOT NULL,
  `LastActivationRequest` int(11) NOT NULL,
  `LostPasswordRequest` int(1) NOT NULL DEFAULT '0',
  `Active` int(1) NOT NULL,
  `Group_ID` int(11) NOT NULL,
  `SignUpDate` int(11) NOT NULL,
  `LastSignIn` int(11) NOT NULL,
  `ChatBanned` int(11) DEFAULT '0',
  `BannedBy` varchar(55) DEFAULT NULL,
  `Shares` int(11) DEFAULT NULL,
  `Banned` int(11) DEFAULT NULL,
  `LastTimeSeen` datetime DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=902 ;

CREATE TABLE IF NOT EXISTS `usersactive` (
`id` int(11) NOT NULL,
  `users_logged_in` int(11) DEFAULT NULL,
  `total_users` int(25) NOT NULL DEFAULT '0',
  `last_update` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `user_addresses` (
  `User_ID` int(11) DEFAULT NULL,
  `Address` text,
  `Wallet_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `wallets` (
  `Name` text NOT NULL,
  `Acronymn` text NOT NULL,
  `Market_Id` int(6) NOT NULL,
  `Wallet_IP` text NOT NULL,
  `Wallet_Username` text NOT NULL,
  `Wallet_Password` text NOT NULL,
`Id` int(11) NOT NULL,
  `Last_Trade` text NOT NULL,
  `Wallet_Port` int(11) NOT NULL,
  `disabled` int(11) NOT NULL DEFAULT '0',
  `Fee` float NOT NULL DEFAULT '0',
  `Minimum_Withdrawal` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=122 ;

CREATE TABLE IF NOT EXISTS `withdraw_history` (
  `Timestamp` text NOT NULL,
  `User` int(11) NOT NULL,
  `Amount` text NOT NULL,
`id` int(11) NOT NULL,
  `Address` text NOT NULL,
  `Coin` text NOT NULL,
  `Buyer` int(11) NOT NULL DEFAULT '0',
  `Seller` int(11) NOT NULL DEFAULT '0',
  `Fee` text
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1062 ;

CREATE TABLE IF NOT EXISTS `withdraw_requests` (
  `Amount` text NOT NULL,
  `Address` text NOT NULL,
  `User_Id` int(11) NOT NULL,
  `Wallet_Id` int(11) NOT NULL,
`Id` int(11) NOT NULL,
  `Account` varchar(55) NOT NULL,
  `CoinAcronymn` varchar(55) NOT NULL,
  `Confirmation` varchar(55) NOT NULL,
  `Confirmed` int(11) NOT NULL DEFAULT '0',
  `Fee` text
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1082 ;

CREATE TABLE IF NOT EXISTS `wrong_fee_canceled_trades` (
  `Id` int(11) NOT NULL DEFAULT '0',
  `To` text CHARACTER SET latin1 NOT NULL,
  `From` text CHARACTER SET latin1 NOT NULL,
  `Amount` text CHARACTER SET latin1 NOT NULL,
  `Value` double NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Type` text CHARACTER SET latin1 NOT NULL,
  `Finished` int(11) NOT NULL DEFAULT '0',
  `Fee` text CHARACTER SET latin1 NOT NULL,
  `Total` text CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `access_violations`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `api_keys`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `balances`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `balance_history`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `bantables_ip`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `canceled_trades`
 ADD PRIMARY KEY (`Id`);

ALTER TABLE `config`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `deposits`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `messages`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `ticketreplies`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `tickets`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `trades`
 ADD PRIMARY KEY (`Id`);

ALTER TABLE `trade_history`
 ADD PRIMARY KEY (`trade_id`);

ALTER TABLE `transfers`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `usercake_groups`
 ADD PRIMARY KEY (`Group_ID`);

ALTER TABLE `usercake_users`
 ADD PRIMARY KEY (`User_ID`);

ALTER TABLE `usersactive`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `wallets`
 ADD PRIMARY KEY (`Id`), ADD UNIQUE KEY `Id` (`Id`);

ALTER TABLE `withdraw_history`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `withdraw_requests`
 ADD PRIMARY KEY (`Id`);


ALTER TABLE `access_violations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1622;
ALTER TABLE `api_keys`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=362;
ALTER TABLE `balances`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27352;
ALTER TABLE `balance_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3122;
ALTER TABLE `bantables_ip`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
ALTER TABLE `canceled_trades`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4342;
ALTER TABLE `config`
MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
ALTER TABLE `deposits`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6492;
ALTER TABLE `messages`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1622;
ALTER TABLE `ticketreplies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=249;
ALTER TABLE `tickets`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
ALTER TABLE `trades`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4412;
ALTER TABLE `trade_history`
MODIFY `trade_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2242;
ALTER TABLE `transfers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
ALTER TABLE `usercake_groups`
MODIFY `Group_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `usercake_users`
MODIFY `User_ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=902;
ALTER TABLE `usersactive`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `wallets`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=122;
ALTER TABLE `withdraw_history`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1062;
ALTER TABLE `withdraw_requests`
MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1082;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
