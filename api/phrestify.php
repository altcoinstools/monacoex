<?php

class Phrestify {

  private $gets = [];
  
  function get($path, $callback) {
    if (substr($path, 0, 1) == '/') {
      $path = substr($path, 1);
    }
    
    $this->gets[$path] = $callback;
  }

  function parse_($path_array, $array) {
    foreach ($this->gets as $path => $callback) {
      $hash = [];
      $path_array1 = explode('/', $path);
      if (count($path_array1) == count($path_array)) {
	for($i = 0; $i < count($path_array); $i++) {
	  $path1 = $path_array1[$i];
	  if (substr($path1, 0, 1) == ':') {
	    $hash[substr($path1, 1)] = $path_array[$i];
	  } else if ($path1 != $path_array[$i]) {
	    break;
	  }
	}
        if ($i == count($path_array)) {
          return call_user_func($callback, $hash);
	}
      }
    }
    return null;
  }

  function parse() {
    $path = $_SERVER['REQUEST_URI'];
    if (substr($path, 0, 1) == '/') {
      $path = substr($path, 1);
    }
    $path_array = explode('/', $path);

    $result = $this->parse_($path_array, $this->gets);
    if ($result == null) {
      header('HTTP', true, 404);
      $result = [ 'error' => [ 'message' => "not found." ]];
    }
    return $result;
  }
}

/*
function callback($hash) {
  return $hash;
}

$phrestify = new Phrestify();
$phrestify->get('/foo/bar/:baz', 'callback');
$phrestify->get('/hoge/:fuga/:fugu', 'callback');

var_dump($phrestify->parse('/foo/barz/12'));
var_dump($phrestify->parse('/foo/bar/12/'));
var_dump($phrestify->parse('/foo/bar/12'));
var_dump($phrestify->parse('/hoge/good/bad'));
*/
?>
