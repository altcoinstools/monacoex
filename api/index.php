<?php
require_once(__DIR__.'/phrestify.php');

$phrestify = new Phrestify();

function callback($hash)
{
  $msg["message"] = $hash['foo'];

  return $msg;
}

$phrestify->get('/api/v0/:foo', 'callback');
echo json_encode($phrestify->parse());
?>
