<?php
if (getenv("_DEBUG")) {
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
}
$title="MonacoEx";
$maint_url = "system/maintenance.php";
date_default_timezone_set('Asia/Tokyo');


require_once(__DIR__.'/../vendor/autoload.php');
require_once("settings.php");
require_once('mysqli.class.php');

if(!isset($language)) $langauge = "en";
require_once("lang/".$langauge.".php");
require_once("jsonRPCClient.php");
require_once("class.user.php");
require_once("class.mail.php");
require_once("funcs.user.php");
require_once("funcs.general.php");
require_once("class.newuser.php");
include("class.wallet.php");

session_start();
if(isset($_SESSION["userCakeUser"]) && is_object($_SESSION["userCakeUser"])) {
	$loggedInUser = $_SESSION["userCakeUser"];
}

$db_config = array();
$db_config['host'] = $db_host;
$db_config['user'] = $db_user;
$db_config['pass'] = $db_pass;
$db_config['table'] = $db_name;
$db = new DB($db_config) or die("Unable to connect to the database");

$oauth_config = [
    'security_salt' => 'probably_monamona',
    'path' => '/auth/',
    'callback_url' => '{path}callback.php',
 
    'Strategy' => [
/* Twitter don't provide Email address ... 
        'Twitter' => [
            'key' => getenv('TWITTER_API_KEY'),
            'secret' => getenv('TWITTER_API_SECRET')
        ],
*/
        'Facebook' => [
            'app_id' => getenv('FACEBOOK_APP_ID'),
            'app_secret' => getenv('FACEBOOK_APP_SECRET'),
            'scope' => 'email'
        ],
	'GitHub' => [
	    'client_id' => getenv('GITHUB_CLIENT_ID'),
	    'client_secret' => getenv('GITHUB_CLIENT_SECRET'),
	    'scope' => 'user:email'
	],
        'Google' => [
            'client_id' => getenv('GOOGLE_CLIENT_ID'),
            'client_secret' => getenv('GOOGLE_CLIENT_SECRET')
	],
	'Yahoojp' => [
	    'client_id' => getenv('YAHOOJP_CLIENT_ID'),
	    'client_secret' => getenv('YAHOOJP_CLIENT_SECRET')
	]
    ]
];
?>
