<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1'); 
class Trade {
	public $trade_value;
	public $trade_amount;
	public $trade_type;
	public $trade_id = 0;
	public $trade_to;
	public $trade_from;
	public $trade_fees;
	public $trade_owner;
	public $trade_total;
	public $etrade_owner;
	public $etrade_type;
	public $etrade_id = 0;
	public $etrade_value;
	public $etrade_amount;
	public $standard;
	public $standarde;
	public $sold;

	public function UpdateTrade() {
		global $db;

		$id = $this->trade_id;
		$amount = sprintf("%.8f",$this->trade_amount);
		if($id == 0 && $amount > 0.0) {
			$trade_to = $this->trade_to;
			$trade_from = $this->trade_from;
			$trade_value = $this->trade_value;
			$trade_owner = $this->trade_owner;
			$trade_type = $this->trade_type;
			$trade_fees = $this->trade_fees;
			$trade_total = $this->trade_total;
			$db->query("INSERT INTO trades (`To`,`From`,`Amount`,`Value`,`User_ID`,`Type`,`Fee`,`Total`)VALUES ('$trade_to','$trade_from','$amount','$trade_value','$trade_owner','$trade_type','$trade_fees','$trade_total');");
		} else {
			if(sprintf("%.8f", $this->trade_amount) == 0.0) {
				$db->query("DELETE FROM Trades WHERE `Id`='$id'");
			} else {
				$db->query("UPDATE Trades SET `Amount`='$amount', `Fee`='$trade_fees` WHERE `Id`='$id'");
			}
		}

	}

	public function UpdateETrade() {
		global $db;

		$id = $this->etrade_id;
		if($id != 0) {
			$amount = sprintf("%.8f",$this->etrade_amount);
			if($amount == 0.0) {
				$db->query("DELETE FROM Trades WHERE `Id`='$id'");
			} else {
				$fee = sprintf("%.8f", $this->etrade_fees);
				$db->query("UPDATE Trades SET `Amount`='$amount', `Fee`='$fee' WHERE `Id`='$id'");
			}
		}
	}

	function Trade($id = 0) {
		global $db;

		if ($id != 0) {
			$tradesql = $db->query("SELECT * FROM Trades WHERE `Id`='$id' LIMIT 1");
			$row_ = $trade_sql->fetch_assoc();
			$this->trade_value = $row_['Value'];
			$this->trade_amount = $row_['Amount'];
			$this->standard = $row_['Amount'];
			$this->trade_id = $id;
			$this->trade_from = $row_['From'];
			$this->trade_to = $row_['To'];
			$this->trade_fees = $row_['Fee'];
			$this->trade_owner = $row_['User_ID'];
			$this->trade_type = $row_['Type'];
		}
	}

	public function GetEquivalentTrade() {
		global $db;

		if($this->trade_from == $this->trade_type) {
			$from = $this->trade_from;
			$value = $this->trade_value;
			$type = $this->trade_type;
			$tradesql2 = $db->query("SELECT * FROM trades WHERE `To` = '$from' AND `Value` >= '$value' AND `Type`='$type' ORDER BY `Value` ASC LIMIT 1");
			$row_ = $tradesql2->fetch_assoc();
			$this->etrade_id = $row_['Id'];
			$this->etrade_value = $row_['Value'];
			$this->etrade_amount = $row_['Amount'];
			$this->standarde = $row_['Amount'];
			$this->etrade_owner = $row_['User_ID'];
			$this->etrade_to = $row_['To'];
			$this->etrade_from = $row_['From'];
			$temp = $row_['Type'];
			$this->etrade_type = $temp;
			$this->etrade_fees = $row_['Fee'];
			//Sell
		} else {
			//Buy
			echo "<br/>" . $this->trade_value;
			$from = $this->trade_from;
			$value = $this->trade_value;
			$type = $this->trade_type;
			$tradesql2 = $db->query("SELECT * FROM trades WHERE `To` = '$from' AND `Value` <= '$value' AND `Type`='$type' LIMIT 1");
			$row_ = $tradesql2->fetch_assoc();
			$this->etrade_id = $row_['Id'];
			$this->etrade_value = $row_['Value'];

			$this->etrade_amount = $row_['Amount'];
			$this->standarde = $row_['Amount'];
			$this->etrade_owner = $row_['User_ID'];
			$this->etrade_to = $row_['To'];
			$this->etrade_from = $row_['From'];
			$this->etrade_fees = $row_['Fee'];
			$temp = $row_['Type'];

			$this->etrade_type = $temp;
		}

	}

	public function ExecuteTrade() {
		global $db;

		$buyer = "";
		$seller = "";
		$buyfee = 0;
		$fee = 0;
		$sellcoin = "";
		if($this->etrade_id != NULL) {
			if($this->trade_from == "MONA") {
				$buyer = $this->trade_owner;
				$seller = $this->etrade_owner;
				$buyfee = $this->trade_fees;
				$sellcoin = $this->etrade_type;
			} else {
				$buyer = $this->etrade_owner;
				$seller = $this->trade_owner;
				$buyfee = $this->etrade_fees;
				$sellcoin = $this->trade_type;
			}
			$price = 0;
			$price2 = 0;
			if($this->trade_from == $this->trade_type) //Sell Order ---------------------------------------------------
			{
				$price2 = $this->etrade_value;
				$price = $this->trade_value;
				$difference = sprintf("%.8f", $this->trade_amount - $this->etrade_amount);
				if ($difference < 0.0) {
					$newamount = 0 - $difference;
					$this->sold = $this->etrade_amount - $newamount;
				} else if ($difference > 0.0) {
					$this->sold = $this->etrade_amount;
				} else if ($difference == 0) {
					$this->sold = $this->etrade_amount;
				}
			} else //Buy Order ---------------------------------------------------
			{

				$price = $this->etrade_value;
				$price2 = $this->trade_value;
				$difference = sprintf("%.8f", $this->etrade_amount - $this->trade_amount);
				if ($difference < 0.0) {
					$newamount = 0 - $difference;
					$this->sold = $this->trade_amount - $newamount;
				} else if ($difference > 0.0) {
					$this->sold = $this->trade_amount;
				} else if ($difference == 0) {
					$this->sold = $this->trade_amount;
				}
			}
			$type = $this->trade_type;
			$sql_112 = $db->query("SELECT Id, Fee FROM Wallets WHERE `Acronymn`='$type' LIMIT 1");
			$row_ = $sql_112->fetch_assoc();
			$type_id = $row_['Id'];
			$feecost = $row_['Fee'];
			$fees = sprintf("%.8f",($this->sold * $price) * $feecost);
			if ($fees < 0.0) {
				$fees=0;
				$feecost=0;
			}
			if ($price2 > $price) {
				$refund = (($this->sold * $price2) + $buyfee) - ($this->sold * $price * (1 + $feecost));
				$refund2 = ($refund) * (1-$feecost);
				AddMoney($refund2,$buyer,"MONA");
			}

			$amtsr = $this->sold * $price;
			AddMoney($amtsr - $fees, $seller, "MONA");

			AddMoney($this->sold, $buyer, $sellcoin);
			/* NOTE: Buyer's fee has already held. */

			AddMoney($fees * 2, -12, "MONA");

			$quantity = $this->sold;
			$time = time();
			$db->query("INSERT INTO Trade_History (`Market_Id`,`Price`,`Quantity`,`Timestamp`,`Buyer`,`Seller`,`S_Receive`,`B_Receive`)VALUES ('$type_id','$price','$quantity','$time','$buyer','$seller','$amtsr','$quantity');");
			if (sprintf("%.8f", $this->etrade_amount - $this->sold) > 0.0) {
				$this->etrade_amount = $this->etrade_amount - $this->sold;
				$this->etrade_fees = $this->etrade_amount * $feecost;
			} else {
				$this->etrade_amount = 0;
				$this->etrade_fees = 0;
			}
			if(sprintf("%.8f", $this->trade_amount - $this->sold) > 0.0) {
				$this->trade_amount = $this->trade_amount - $this->sold;
				$this->trade_fees = $this->trade_amount * $feecost;
			} else {
				$this->trade_amount = 0;
				$this->trade_fees = 0;
			}
		}
		$this->UpdateTrade();
		$this->UpdateETrade();
	}
}
?>
