<?php
	function isUserAdmin($user)
	{
		global $db;

		$sql94 = $db->query("SELECT `Group_ID` FROM userCake_Users WHERE `User_ID`='$user'");
		$row_ = $sql94->fetch_assoc();
		if ($row_['Group_ID'] == 9) {
			return true;
		} else {
			return false;
		}
	}
	
	function isUserMod($user)
	{
		global $db;

		$sql93 = $db->query("SELECT `Group_ID` FROM userCake_Users WHERE `User_ID`='$user'");
		$row_ = $sql93->fetch_assoc();
		if ($row_['Group_ID'] == 5) {
			return true;
		} else {
			return false;
		}
	}
	
	function isUserNormal($user)
	{
		global $db;

		$sql92 = $db->query("SELECT `Group_ID` FROM userCake_Users WHERE `User_ID`='$user'");
		$row_ = $sql92->fetch_assoc();
		if ($row_['Group_ID'] == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	function isUserCBanned($user)
	{
		global $db;

		$sql91 = $db->query("SELECT `ChatBanned` FROM userCake_Users WHERE `User_ID`='$user'");
		$row_ = $sql91->fetch_assoc();
		if ($row_['ChatBanned'] == 1) {
			return true;
		} else {
			return false;
		}
	}

	function isBant($user)
	{
		global $db;

		$sqlxG = $db->query("SELECT `Banned` FROM userCake_Users WHERE `User_ID`='$user'");
		$row_ = $sqlxG->fetch_assoc();
		if ($row_['Banned'] == 1) {
			return true;
		} else {
			return false;
		}
	}

	function lockToIP($account,$ip){
		global $db;

		$sqlquery = $db->query("UPDATE `userCake_Users` SET `lockip`='$ip' WHERE `Username`='$account'");
		if($db->error($sqlquery)) {
			return false;
		}else{
			return true;
		}
	}

	function usernameExists($username)

	{

		global $db,$db_table_prefix;

		

		$sql = "SELECT Active

				FROM ".$db_table_prefix."Users

				WHERE

				Username_Clean = '".$db->real_escape_string(sanitize($username))."'

				LIMIT 1";

	

		if(returns_result($sql) > 0)

			return true;

		else

			return false;

	}

	

	function emailExists($email)

	{

		global $db,$db_table_prefix;

	

		$sql = "SELECT Active FROM ".$db_table_prefix."Users

				WHERE

				Email = '".$db->real_escape_string(sanitize($email))."'

				LIMIT 1";

	

		if(returns_result($sql) > 0)

			return true;

		else

			return false;	

	}

	

	//Function lostpass var if set will check for an active account.

	function validateActivationToken($token,$lostpass=NULL)

	{

		global $db,$db_table_prefix;

		

		if($lostpass == NULL) 

		{	

			$sql = "SELECT ActivationToken

					FROM ".$db_table_prefix."Users

					WHERE Active = 0

					AND

					ActivationToken ='".$db->real_escape_string(trim($token))."'

					LIMIT 1";

		}

		else 

		{

			 $sql = "SELECT ActivationToken

			 		FROM ".$db_table_prefix."Users

					WHERE Active = 1

					AND

					ActivationToken ='".$db->real_escape_string(trim($token))."'

					AND

					LostPasswordRequest = 1 LIMIT 1";

		}

		

		if(returns_result($sql) > 0)

			return true;

		else

			return false;

	}

	

	

	function setUserActive($token)

	{

		global $db,$db_table_prefix;

		

		$sql = "UPDATE ".$db_table_prefix."Users

		 		SET Active = 1

				WHERE

				ActivationToken ='".$db->real_escape_string(trim($token))."'

				LIMIT 1";

		

		return ($db->query($sql));

	}

	

	//You can use a activation token to also get user details here

	function fetchUserDetails($username=NULL,$token=NULL)

	{

		global $db,$db_table_prefix; 

		

		if($username!=NULL) 

		{  

			$sql = "SELECT * FROM ".$db_table_prefix."Users

					WHERE

					Username_Clean = '".$db->real_escape_string(sanitize($username))."'

					LIMIT

					1";

		}

		else

		{

			$sql = "SELECT * FROM ".$db_table_prefix."Users

					WHERE 

					ActivationToken = '".$db->real_escape_string(sanitize($token))."'

					LIMIT 1";

		}

		 

		$result = $db->query($sql);
		$row = $result->fetch_assoc();

		return ($row);

	}

	

	function flagLostPasswordRequest($username,$value)

	{

		global $db,$db_table_prefix;

		

		$sql = "UPDATE ".$db_table_prefix."Users

				SET LostPasswordRequest = '".$value."'

				WHERE

				Username_Clean ='".$db->real_escape_string(sanitize($username))."'

				LIMIT 1

				";

		

		return ($db->query($sql));

	}

	

	function updatePasswordFromToken($pass,$token)

	{

		global $db,$db_table_prefix;

		

		$new_activation_token = generateActivationToken();

		

		$sql = "UPDATE ".$db_table_prefix."Users

				SET Password = '".$db->real_escape_string($pass)."',

				ActivationToken = '".$new_activation_token."'

				WHERE

				ActivationToken = '".$db->real_escape_string(sanitize($token))."'";

		

		return ($db->query($sql));

	}

	

	function emailUsernameLinked($email,$username)

	{

		global $db,$db_table_prefix;

		

		$sql = "SELECT Username,

				Email FROM ".$db_table_prefix."Users

				WHERE Username_Clean = '".$db->real_escape_string(sanitize($username))."'

				AND

				Email = '".$db->real_escape_string(sanitize($email))."'

				LIMIT 1

				";

		

		if(returns_result($sql) > 0)

			return true;

		else

			return false;

	}

	

	

	function isUserLoggedIn()

	{

		global $loggedInUser,$db,$db_table_prefix;
		
		if($loggedInUser == NULL)

		{

			return false;

		}

		else

		{

		$sql = "SELECT User_ID,

				Password

				FROM ".$db_table_prefix."Users

				WHERE

				User_ID = '".$db->real_escape_string($loggedInUser->user_id)."'

				AND 

				Password = '".$db->real_escape_string($loggedInUser->hash_pw)."' 

				AND

				Active = 1

				LIMIT 1";

		

		

			//Query the database to ensure they haven't been removed or possibly banned?

			if(returns_result($sql) > 0)

			{

					return true;

			}

			else

			{

				//No result returned kill the user session, user banned or deleted

				$loggedInUser->userLogOut();

			

				return false;

			}

		}

	}

	

	//This function should be used like num_rows, since the PHPBB Dbal doesn't support num rows we create a workaround

	function returns_result($sql)

	{

		global $db;

		

		$count = 0;

		$result = $db->query($sql);
		return $result->num_rows;
	}

	

	//Generate an activation key 

	function generateActivationToken()

	{

		$gen;

	

		do

		{

			$gen = md5(uniqid(mt_rand(), false));

		}

		while(validateActivationToken($gen));

	

		return $gen;

	}

	

	function updateLastActivationRequest($new_activation_token,$username,$email)

	{

		global $db,$db_table_prefix; 

		

		$sql = "UPDATE ".$db_table_prefix."Users

		 		SET ActivationToken = '".$new_activation_token."',

				LastActivationRequest = '".time()."'

				WHERE Email = '".$db->real_escape_string(sanitize($email))."'

				AND

				Username_Clean = '".$db->real_escape_string(sanitize($username))."'";

		

		return ($db->query($sql));

	}

?>
