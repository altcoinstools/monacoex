<?php
	//Application Db
	$dbtype = "mysql"; 
        $env = getenv("CLEARDB_DATABASE_URL");
        if ($env) {
          # Heroku style
          $db_url = parse_url(getenv("CLEARDB_DATABASE_URL"));
          $db_host = $db_url["host"];
          $db_user = $db_url["user"];
          $db_pass = $db_url["pass"];
          $db_name = substr($db_url["path"], 1);
        } else{
          # Azure style
          foreach ($_SERVER as $key => $value) {
            if (strpos($key, "MYSQLCONNSTR_") !== 0) {
              continue;
            }

            $db_host = preg_replace("/^.*Data Source=(.+?);.*$/", "\\1", $value);
            $db_user = preg_replace("/^.*User Id=(.+?);.*$/", "\\1", $value);
            $db_pass = preg_replace("/^.*Password=(.+?)$/", "\\1", $value);
            $db_name = preg_replace("/^Database=(.+?);.*$/", "\\1", $value);
            break;
          }
        }
	$db_port = "";

	$db_wallet_user = "username";
	$db_wallet_password = "password";
	$db_table_prefix = "userCake_";
	$langauge = "en";

	

	//Generic website variables
	$websiteName = "MonacoEx";
	$websiteUrl = (empty($_SERVER['HTTPS']) ? "http" : "https").'://'.$_SERVER['HTTP_HOST'].'/'; //including trailing slash
	$emailActivation = false;
	$resend_activation_threshold = 24;
	$emailAddress = "admin@monaco-ex.org";
        $emailUsername = getenv("SENDGRID_USERNAME");
        $emailPassword = getenv("SENDGRID_PASSWORD");
	$emailHost = "smtp.sendgrid.com";
	$emailPort = 587;
	$emailDate = date("l \\t\h\e jS");
	$mail_templates_dir = "models/mail-templates/";
	$default_hooks = array("#WEBSITENAME#","#WEBSITEURL#","#DATE#");
	$default_replace = array($websiteName,$websiteUrl,$emailDate);
	$debug_mode = true;
	//---------------------------------------------------------------------------

	$twitter_id = "MonacoEx"

?>
