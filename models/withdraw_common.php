<?php
require_once("models/config.php");
function withdraw_approve($request)
{
	global $db;

	$vars = $db->query("SELECT * FROM `Withdraw_Requests` WHERE `Id`='$request'");
	if ($vars->num_rows >= 1) {
		$row_ = $vars->fetch_assoc();
		$confirmed = $row_['Confirmed'];
		if($confirmed == 1)
		{
			$address = $row_['Address'];
			$total = $row_['Amount'];
			$user = $row_['User_Id'];
			$w_id = $row_['Wallet_Id'];
			$coin = $row_['CoinAcronymn'];
			$fee  = $row_['Fee'];
			$wallet = new Wallet($w_id);
			if ($wallet->Withdraw($address,$total,$fee,$user,$coin)) {
				$db->query("UPDATE Withdraw_Requests SET Confirmed = 2 WHERE `Id`='$request'");
				AddMoney($fee,-5,$w_id);
				return true;
			}
		}
	}
	return false;
}

function withdraw_cancel($request, $debug = false)
{
	global $db;

	$vars = $db->query("SELECT * FROM Withdraw_Requests WHERE `Id`='$request'");
	$row_ = $vars->fetch_assoc();
	$user   = $row_['User_Id'];
	$w_id   = $row_['Wallet_Id'];
	$coin   = $row_['CoinAcronymn'];
	$refund = sprintf('%.8f', $row_['Amount'] + $row_['Fee']);

	$sqlget = $db->query("SELECT * FROM balances WHERE `User_Id`='$user' AND `Wallet_ID`='$w_id' AND `Coin`='$coin'");
	if($sqlget->num_rows == 1) {
		$row_ = $sqlget->fetch_assoc();
		$newbal = $row_['Amount'] + $refund;
		$request_id = $row_['id'];
		$finish = $db->query("UPDATE balances SET `Amount`='$newbal' WHERE `id`='$request_id'");
		if($finish != null){
			$delete = $db->query("UPDATE Withdraw_Requests SET Confirmed = -2 WHERE `Id`='$request'");
			if($delete != null){
				return true;
			}
			if ($debug) {
				echo 'problem with query :'.$db->error($delete);
			}
		}else{
			if ($debug) {
				echo 'problem with query :'.$db->error($finish);
			}
		}
	}else{
		echo "Internal error. Contact site admin.";
	}

	return false;
}
