<?php
	/*
		UserCake Langauge File.
		Language: Japanese.
		Author: Masaki "monaka" Muranaka
		http://monaka.org/
		License: MIT License
	*/
	
	/*
		%m1% - Dymamic markers which are replaced at run time by the relevant index.
	*/

	$lang = array();
	
	//Account
	$lang = array_merge($lang,array(
		"ACCOUNT_SPECIFY_USERNAME" 				=> "ユーザ名を入力してください",
		"ACCOUNT_SPECIFY_PASSWORD" 				=> "パスワードを入力してください",
		"ACCOUNT_SPECIFY_EMAIL"					=> "Eメールアドレスを入力してください",
		"ACCOUNT_INVALID_EMAIL"					=> "不正なEメールアドレスです",
		"ACCOUNT_INVALID_USERNAME"				=> "不正なユーザ名です",
		"ACCOUNT_USER_OR_EMAIL_INVALID"			=> "ユーザ名かEメールアドレスが不正です",
		"ACCOUNT_USER_OR_PASS_INVALID"			=> "ユーザ名かパスワードが不正です",
		"ACCOUNT_ALREADY_ACTIVE"				=> "アカウントは既にアクティベートされています",
		"ACCOUNT_INACTIVE"						=> "アカウントはアクティベート中です． アクティベーションの手順を記したEメールを確認して下さい(もしかしたら迷惑メールフォルダにあるかもしれません)",
		"ACCOUNT_USER_CHAR_LIMIT"				=> "ユーザ名は %m1% 文字以上 %m2% 文字以下にしてください",
		"ACCOUNT_PASS_CHAR_LIMIT"				=> "パスワードは %m1% 文字以上 %m2% 文字以下にしてください",
		"ACCOUNT_PASS_MISMATCH"					=> "パスワードが一致しません",
		"ACCOUNT_USERNAME_IN_USE"				=> "ユーザ名 %m1% は既に使われてます",
		"ACCOUNT_EMAIL_IN_USE"					=> "Eメールアドレス %m1% は既に使われています",
		"ACCOUNT_LINK_ALREADY_SENT"				=> "アクティベーション用メールは，約 %m1% 時間前に，このEメールアドレスに送信されました",
		"ACCOUNT_NEW_ACTIVATION_SENT"			=> "新しいアクティベーションのためのリンクをEメールで送信しました．確認して下さい．",
		"ACCOUNT_NOW_ACTIVE"					=> "アカウントはアクティブです．",
		"ACCOUNT_SPECIFY_NEW_PASSWORD"			=> "新しいパスワードを入力してください．",
		"ACCOUNT_NEW_PASSWORD_LENGTH"			=> "新しいパスワードは %m1% 文字以上 %m2% 文字以下にしてください",
		"ACCOUNT_PASSWORD_INVALID"				=> "現在のパスワードに誤りがあります",	
		"ACCOUNT_EMAIL_TAKEN"					=> "このEメールアドレスは他のユーザによって取得されています",
		"ACCOUNT_DETAILS_UPDATED"				=> "アカウントの詳細が更新されました",
		"ACTIVATION_MESSAGE"					=> "ログインの前に，アカウントをアクティベートする必要があります．\n\n
													%m1%activate-account.php?token=%m2%",							
		"ACCOUNT_REGISTRATION_COMPLETE_TYPE1"	=> "登録が完了しました．<a href=\"login.php\">このリンク</a>からログイン可能です．",
		"ACCOUNT_REGISTRATION_COMPLETE_TYPE2"	=> "登録が完了しました．アクティベーションのためのEメールを送信しました．\n
													ログインの前にアカウントのアクティベートを行ってください．",

		"ACCOUNT_CURRENCY" => "通貨",
		"ACCOUNT_AVAILABLE" => "残高",
		"ACCOUNT_PENDING" => "処理待ち",
		"ACCOUNT_DEPOSIT" => "入金",
		"ACCOUNT_WITHDRAW" => "出金",
		"ACCOUNT_FLUSH" => '残高更新',

		"ACCOUNT_MENU_BALANCES" => "収支",
		"ACCOUNT_MENU_OPEN_ORDERS" => "未約定注文",
		"ACCOUNT_MENU_HISTORY" => "履歴",
		"ACCOUNT_MENU_SETTINGS" => "設定",
		"ACCOUNT_MENU_SUPPORT" => "サポート",
		"ACCOUNT_MENU_API_INFO" => "API情報",

		"LOGGED_IN_AS" => "ログイン済み: %m1%",
		"NOT_LOGGED_IN" => "未ログイン"
	));
	
	// Register
	$lang = array_merge($lang,array(
		"Register" => "ユーザ登録",
		"REGISTER_TOS_REQUIREMENT" =>'<b>ご登録に際しては，<a href="index.php?page=tos"><u> %m1% </u></a></b>にご同意頂く必要があります．',
		"Username contains no Alphanumeric charachters" => "ユーザ名に英文字以外の文字が含まれています．",
		"Username Unavailable" => "ユーザ名が存在しません",
		'Successfully registered! Returning you to the login form!' => '登録完了しました．ログインフォームに戻ります．',
		"PLACEHOLDER_EMAIL" => "E-mail (重要)",
		"PLACEHOLDER_USERNAME" => "ユーザ名",
		"PLACEHOLDER_PASSWORD" => "パスワード",
		"PASSWORD_STRENGTH" => "パスワードの強度: 未入力",
		"PLACEHOLDER_REPEAT_PASSWORD" => "パスワードを再度",
		"PLACEHOLDER_ENTER_SECURITY_CODE" => "セキュリティコードを入力",
		"PLACEHOLDER_RECEIVING_ADDRESS" => "受け取りアドレス",
		"Registering..." => "登録中..."
	));

	//Login
	$lang = array_merge($lang,array(
		"LOGIN_TITLE" => "ログイン",
		"LOGIN_DISABLED" => "ログイン機能を停止しています．",
		"LOGIN_REMEMBER_ME" => "ログイン状態を記憶",
		"LOGGING_IN" => "ログイン中...",
		"LOGIN_FORGOT_PASSWORD" => "Help! パスワードを忘れました"
	));

	//Forgot Password
	$lang = array_merge($lang,array(
		"FORGOTPASS_INVALID_TOKEN"				=> "トークンが不正です",
		"FORGOTPASS_NEW_PASS_EMAIL"				=> "新しいパスワードをEメールで送りました",
		"FORGOTPASS_REQUEST_CANNED"				=> "パスワード紛失のリクエストはキャンセルされました",
		"FORGOTPASS_REQUEST_EXISTS"				=> "このアカウントには，既に未処理のパスワード紛失リクエストがあります．",
		"FORGOTPASS_REQUEST_SUCCESS"			=> "アカウントへの再アクセスを行う手順をEメールで送信しました",
	));

	//Withdraw
	$lang = array_merge($lang,array(
		"WITHDRAW" => '出金',
		"WITHDRAW_DISABLED" => '出金機能は現在無効になっています．',
		"WITHDRAW_CONFIRMATION_SENT" => "確認の電子メールが送信されました．",
		"WITHDRAW_PENDING_AVAILABLE" => '出金は処理待ちになりました',
	));

	//Account history
	$lang = array_merge($lang,array(
		"Your Trade History" => "トレード履歴",
		"Your Deposit History" => "入金履歴",
		"Your Pending Withdrawals" => "処理待ちの出金",
		"Your Withdraw History" => "出金履歴"
	));

	//Miscellaneous
	$lang = array_merge($lang,array(
		"TOS" => "利用許諾条件",
		"MARKET" => "市場",
		"Date" => "日時",
		"Coin" => "通貨",
		"Price" => "価格",
		"Quantity" => "数量",
		"VOLUME" => "出来高",
		"CURRENT_ASK" => "売り気配",
		"CURRENT_BID" => "買い気配",
		"LAST_PRICE" => "終値",
		"UNIT" => "単位",
		"Address" => "アドレス",
		"CAPTCHA_FAIL"							=> "セキュリティコード認証に失敗しました",
		"FAIL_MINIMUM"							=> "最小の払戻額は .01 です",
		"INVALID_AMOUNT"						=> "金額が入力されていません",
		"N_A_N"									=> "数字ではありません!",
		"INS_FUNDS"								=> "残高不足",
		"CONFIRM"								=> "確認",
		"DENY"									=> "拒否",
		"SUCCESS"								=> "成功",
		"ERROR"									=> "エラー",
		"NOTHING_TO_UPDATE"						=> "アップデートはありません",
		"SQL_ERROR"								=> "重大な SQL エラー",
		"MAIL_ERROR"							=> "メール送信に関する重大なエラー．サーバ管理者に問い合わせてください",
		"MAIL_TEMPLATE_BUILD_ERROR"				=> "Eメールテンプレート構築中のエラー",
		"MAIL_TEMPLATE_DIRECTORY_ERROR"			=> "メールテンプレートディレクトリを開けません． メールディレクトリを %m1% にセットしてみてください",
		"MAIL_TEMPLATE_FILE_EMPTY"				=> "テンプレートファイルが空です… 送るものがありません",
		"FEATURE_DISABLED"						=> "この機能は現在無効にされています",
	));
?>
