<?php

/**
* Load config
*/
require_once(dirname(__DIR__).'/models/config.php');

/**
 * Instantiate Opauth with the loaded config but not run automatically
 */
require(dirname(__DIR__).'/vendor/opauth/opauth/lib/Opauth/Opauth.php');
$Opauth = new Opauth( $oauth_config, false );


/**
* Fetch auth response, based on transport configuration for callback
*/
$response = null;

switch($Opauth->env['callback_transport']) {
	case 'session':
		/* session is already started in config.php */
		$response = $_SESSION['opauth'];
		unset($_SESSION['opauth']);
		break;
	case 'post':
		$response = unserialize(base64_decode( $_POST['opauth'] ));
		break;
	case 'get':
		$response = unserialize(base64_decode( $_GET['opauth'] ));
		break;
	default:
		echo '<strong style="color: red;">Error: </strong>Unsupported callback_transport.'."<br>\n";
		break;
}

/**
 * Check if it's an error callback
 */
if (array_key_exists('error', $response)) {
	echo '<strong style="color: red;">Authentication error: </strong> Opauth returns error auth response.'."<br>\n";
}

/**
 * Auth response validation
 * 
 * To validate that the auth response received is unaltered, especially auth response that 
 * is sent through GET or POST.
 */
else{
	if (empty($response['auth']) || empty($response['timestamp']) || empty($response['signature']) || empty($response['auth']['provider']) || empty($response['auth']['uid'])) {
		echo '<strong style="color: red;">Invalid auth response: </strong>Missing key auth response components.'."<br>\n";
	} elseif (!$Opauth->validate(sha1(print_r($response['auth'], true)), $response['timestamp'], $response['signature'], $reason)) {
		echo '<strong style="color: red;">Invalid auth response: </strong>'.$reason.".<br>\n";
	} else {
		echo '<strong style="color: green;">OK: </strong>Auth response is validated.'."<br>\n";

		/**
		 * It's all good. Go ahead with your application-specific authentication logic
		 */
	}
}

//-----------------------------------------------------------------------------------

$debug_mode = getenv('_DEBUG');


if ($debug_mode) {
	var_dump($response);
	echo "<br/>\n";
}

if (isset($_SESSION["userCakeUser"]) && is_object($_SESSION["userCakeUser"])) {
	destroySession("userCakeUser");
}

if (!isset($response["auth"]["info"]["email"])) {
var_dump($response);
	die('cannot get email from the provider.');
} else {
	$email = $response["auth"]["info"]["email"];
	$sql = $db->query("SELECT * FROM ".$db_table_prefix."Users WHERE Email = '".
			$db->real_escape_string(sanitize($email))."'");
	if ($sql->num_rows == 0) {
		if ($debug_mode) {
			echo "New user.<br/>\n";
		}
		$newUser = new User($response["auth"]["provider"].':'.$response["auth"]["uid"],
				generateActivationToken(),
				$email);
		$newUser->userCakeAddUser();
		$sql = $db->query("SELECT * FROM ".$db_table_prefix."Users WHERE Email = '".
				$db->real_escape_string(sanitize($email))."'");
	}

	if ($sql->num_rows != 1) {
		die("Fatal error in callback.php. Please contact server admins.");
	}

	$row = $sql->fetch_assoc();
	$loggedInUser = new loggedInUser();
	$loggedInUser->email = $row["Email"];
	$loggedInUser->user_id = $row["User_ID"];
	$loggedInUser->hash_pw = $row["Password"];
	$loggedInUser->display_username = $row["Username"];
	$loggedInUser->clean_username = $row["Username_Clean"];
	$loggedInUser->updateLastSignIn();

	$_SESSION["userCakeUser"] = $loggedInUser;

	if (getenv('_DEBUG')) {
		echo 'Login complete. Jump to <a href="/index.php?page=home">top page</a>.<br/>'."\n";
	} else {
		redirect_url('/index.php?page=home');
	}
}
?>

