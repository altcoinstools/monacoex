<?php
include("../models/config.php");

$id     = $db->real_escape_string($_GET["market"]);
$g = 0;
$sql = $db->query("SELECT t.Value AS Value, SUM(t.Amount) AS Amount, Amount * Value AS Total FROM trades AS t INNER JOIN Wallets AS w ON w.id = '$id' AND t.`From` = w.Acronymn AND t.`Type`=w.Acronymn GROUP BY t.Value ORDER BY t.Value");
while ($row = $sql->fetch_assoc()) {
	$g++;
	if($g & 1) {
		$color = "lightgray";
	} else {
		$color = "darkgray";
	}
?>
<tr style="cursor: pointer;" title="Click To Fill Order Form" class="<?php echo $color; ?>" onclick="document.getElementById('Amount2').value = '<?php echo $row["Total"]; ?>'; document.getElementById('price2').value = '<?php echo $row["Value"]; ?>'; calculateFees2(this);">
	<td class="value"><?php echo sprintf("%.8f", $row["Value"]); ?></td>
	<td class="value"><?php echo sprintf("%.8f", $row["Amount"]); ?></td>
	<td class="value"><?php echo sprintf("%.8f", $row["Total"]); ?></td>
</tr>
<?php
}
?>
