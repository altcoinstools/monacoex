<?php
if(!isUserLoggedIn()) {
  redirect_url('index.php?page=login');
}
if (!isset($_GET['fchk'])){
  redirect_url('index.php?page=account');
}

$account = $loggedInUser->display_username;

$acronymn = $db->real_escape_string($_GET['fchk']);
$sql = $db->query("SELECT * FROM Wallets WHERE `disabled`='0' AND `Acronymn` = '$acronymn' ORDER BY `Id` ASC");
?>
<h2>Receive status (<?php echo $acronymn; ?>)</h2>
<table class="page data">
  <thead>
    <tr>
      <th>Amount</th>
      <th>Status</th>
      <th>TxID</th>
    </tr>
  </thead>
  <tbody>
<?php
while ($row = $sql->fetch_assoc()) {
  $id = $row["Id"];
  $wallet = new Wallet($id);
  $listtrans = $wallet->Client->listtransactions($account, 5);
  if ($acronymn=="BTC"){
    $listtrans = $listtrans["transactions"];
  }
  foreach($listtrans as $key => $trans) {
    if ($trans["category"] != 'receive') {
      continue;
    }

    $txid = $trans['txid'];
    $txid_short = substr($txid, 0, 8).'...';
    $acronymn=$acronymn;
    $account = $trans["account"];
    $confirms = $trans["confirmations"];
    $amount = $trans["amount"];
    $sql2 = @$db->query("SELECT * FROM deposits WHERE `Transaction_Id`='$txid' AND `Coin`='$acronymn'");
    $row_ = $sql2->fetch_assoc();
    $id2 = $row_['id']; /* $sql2 */;
    $paid = $row_['Paid']; /* $sql2 */;

    if($id2 != NULL) {
      if($paid == 0) {
	if($confirms > 0 && $account != "") {
          $db->query("UPDATE deposits SET `Paid`='1' WHERE `id`='$id2'");
          AddMoney($amount, $account, $acronymn);
          $status = 'Credited now';
        }else{
          $status = 'Uncomfirmed ('.$confirms.' of 1)';
        }
      } else {
        $status = "Already credited";
      }
    } else {
      if($account != "") {
        if($confirms > 5) {
          $db->query("INSERT INTO  deposits (`Transaction_Id`,`Amount`,`Coin`,`Paid`,`Account`) VALUES ('$txid','$amount','$acronymn','1','$account');");
          AddMoney($amount, $account, $acronymn);
          $status = 'Credited now';
        }else{
          $db->query("INSERT INTO  deposits (`Transaction_Id`,`Amount`,`Coin`,`Paid`,`Account`) VALUES ('$txid','$amount','$acronymn','0','$account');");
          $status = 'Uncomfirmed ('.$confirms.' of 6)';
        }
      }
    }
?>
  <tr>
    <td class="value"><?php echo sprintf("%.8f", $amount); ?></td>
    <td><?php echo $status; ?></td>
    <td title="<?php echo $txid; ?>"><?php echo $txid_short; ?></td>
  </tr>
<?php
  }
}
?>
  </tbody>
</table>
<a href="index.php?page=account">Back to account.</a>
