<?php
function update_confirm_or_deny($db, $id, $crt, $newval)
{
  $sql       = $db->query("SELECT * FROM Withdraw_Requests WHERE `Confirmation` = '$crt' AND `User_Id`='$id'");
  $row_      = $sql->fetch_assoc();
  $crt2      = $row_['Confirmation']; /* $sql */;
  $id2       = $row_['Id']; /* $sql */;
  $confirmed = $row_['Confirmed']; /* $sql */;
  if ($confirmed != null) {
    if ($confirmed == "0") {
      $db->query("UPDATE `Withdraw_Requests` SET `Confirmed`='$newval' WHERE `id`='$id2'");
      $m = ($newval == -1) ? "canceled" : "confirmed";
      return "Your withdraw has been $m; however, an admin must still process this before your withdraw is processed.";
    } else {
      return "This Withdraw Request Has Already Been Confirmed Or Denied";
    }
  } else {
    return "Invalid Confirmation Key.";
  }
}

function check_confirm_deny($db, $id) {
  if (isset($_GET["confirm"])) {
    if (isset($_GET["deny"])) {
      return  "Invalid Confirmation Request";
    } else {
      $crt = $db->real_escape_string($_GET["confirm"]);
      return update_confirm_or_deny($db, $id, $crt, 1);
    }
  } else if (isset($_GET["deny"])) {
    $crt = $db->real_escape_string($_GET["deny"]);
    return update_confirm_or_deny($db, $id, $crt, -1);
  }
  return ""; /* No message as there is no confirm/deny request. */
}

if (isWithdrawalDisabled()) {
  echo lang("WITHDRAW_DISABLED");
  if ($twitter_id) {
    echo ' See our <a href="https://twitter.com/'.$twitter_id.'">Twitter Account</a> for details.';
  }
} else {
  if (!isUserLoggedIn()) {
    echo 'invalid request : not logged in';
  } else {
    echo check_confirm_deny($db, $id);
  }
}
?>
