<?php
if(!isUserLoggedIn())
{
	redirect_url('index.php?page=login');
}
if(!isUserMod($id) and !isUserAdmin($id))
{
	redirect_url('access_denied.php');
}
$account0 = $loggedInUser->display_username;
?>
<div class="tabber">
	<div class="tabbertab" title="User Stats">
	<table class="page">
			<?php 
			$getusers = $db->query("SELECT * FROM usersactive WHERE `id`=1");
			$row_ = $getusers->fetch_assoc();
			$total = $row_['total_users']; /* $getusers */;
			$upda = $row_['last_update']; /* $getusers */;

			$getloggedin = $db->query("SELECT COUNT(*) AS `count` FROM `userCake_Users` WHERE LastTimeSeen > DATE_SUB(NOW(), INTERVAL 5 MINUTE)");
			$row_ = $getloggedin->fetch_assoc();
			$loggedin = $row_['count']; /* $getloggedin */;

				echo "
				<tr>
					<td>Logged In : ".$loggedin."<td>
				</tr>
				<tr>
					<td>Total Users: ".$total."<td>
				</tr>
				<tr>
					<td>Last Updated : ".$upda."<td>
				</tr>
				";
			
			?>
	</table>
	</div>
	
	<div class="tabbertab" title="Chat Bans">
		<form action="" name="cbanform" method="POST" onsubmit="document.getElementById('#bango').disabled = 1;">
			<input type="text" name="cban" class="field" />
			<input type="submit" value="cban" class="blues" id="bango" onclick="this.disabled=true;this.value='Banning...';this.form.submit();"/>
		</form>	
		<b>Users Banned From Chat</b>
		<table class="page">
			<tr>
				<th>Username</th> 
				<th>Email</th> 
				<th>Role</th>
				<th>Status</th>
				<th>UnBan</th>
			<tr>
		<?php
		$sqlzz = $db->query("SELECT * FROM userCake_Users WHERE `ChatBanned`='1'");
		while ($row = $sqlzz->fetch_assoc()) {
		$group_id = $row["Group_ID"];
		$sqls = $db->query("SELECT * FROM userCake_Groups WHERE `Group_ID`='$group_id'");
		$row_ = $sqls->fetch_assoc();
		$group = $row_['Group_Name']; /* $sqls */;
		$username = $row["Username"];
		echo'
			<tr>
				<td>'.$row["Username"].'</td>
				<td>'.$row["Email"].'</td>
				<td><p>'.$group.'</p></td>
				<td>banned by '.$row["BannedBy"].'</td>
				<td><a href="index.php?page=moderate&cunban='.$row["Username"] . '">Unban</a>
			</tr>';
			}
		?>
		</table>
	</div>
<?php
if (isset($_POST["cban"])) {
	$banby = $account0;
	$username = $db->real_escape_string(strip_tags($_POST["cban"]));
	$db->query("UPDATE userCake_Users SET `ChatBanned`='1' WHERE `Username`='$username'");
	$db->query("UPDATE userCake_Users SET `BannedBy`='$banby' WHERE `Username`='$username'");
}
if (isset($_GET["cunban"])) {
	$username = $db->real_escape_string(strip_tags($_GET["cunban"]));
	$db->query("UPDATE userCake_Users SET `ChatBanned`='0' WHERE `Username`='$username'");
	$db->query("UPDATE userCake_Users SET `BannedBy`=NULL WHERE `Username`='$username'");
	redirect_url('index.php?page=moderate');
}		
?>

<?php 
if(isUserAdmin($id)) {			
	?>
	<div class="tabbertab" title="Site Bans">
	<form action="" name="banuser" method="POST" onsubmit="document.getElementById('#siteban').disabled = 1;">
		<input type="text" name="ban" class="field" />
		<input type="submit" value="ban" class="blues" id="siteban" />
	</form>
	<?php
	if (isset($_POST["ban"])) {
		$banby = $account0;
		$username = $db->real_escape_string(strip_tags($_POST["ban"]));
		$db->query("UPDATE userCake_Users SET `Banned`='1' WHERE `Username`='$username'");
	}
	if (isset($_GET["unban"])) {
		$username = $db->real_escape_string(strip_tags($_GET["unban"]));
		$db->query("UPDATE userCake_Users SET `Banned`='0' WHERE `Username`='$username'");
		redirect_url('index.php?page=moderate');
	}		
	?>
	<b>Users Banned From Logging In</b>
	<table class="page">
		<tr>
			<th>Username</th> 
			<th>Email</th> 
			<th>Role</th>
			<th>UnBan</th>
		<tr>
	<?php
	$sqlzz = $db->query("SELECT * FROM userCake_Users WHERE `Banned`='1'");
	while ($row = $sqlzz->fetch_assoc()) {
		$group_id = $row["Group_ID"];
		$sqls = $db->query("SELECT * FROM userCake_Groups WHERE `Group_ID`='$group_id'");
		$row_ = $sqls->fetch_assoc();
		$group = $row_['Group_Name']; /* $sqls */;
		$username = $row["Username"];
	echo'
		<tr>
			<td>'.$row["Username"].'</td>
			<td>'.$row["Email"].'</td>
			<td><p>'.$group.'</p></td>
			<td><a href="index.php?page=moderate&unban='.$row["Username"] . '">Unban</a>
		</tr>';
		}
	?>
	</table>	
	</div>
<?php
}
?>

