<?php
if(!isUserLoggedIn())
{
	redirect_url('index.php?page=login');
}
$user = addslashes(strip_tags($loggedInUser->user_id));
$act_name = addslashes(strip_tags($loggedInUser->display_username));
?>
<div style="margin: 0 auto;">
<div class="top aligncenter">
<?php echo lang('Your Trade History'); ?>
</div>
<div class="box"">
<table class="page data">
<thead>
<tr class="blue">
	<th><?php echo lang("Date"); ?></th>
	<th><?php echo lang("Coin"); ?></th>
	<th>Type</th>
	<th><?php echo lang("Price"); ?></th>
	<th><?php echo lang("Quantity"); ?></th>	
	<th>Total(MONA)</th>
</tr>
</thead>
<tbody>
<?php
$sqlz = $db->query("SELECT * FROM Trade_History WHERE (`Buyer`='$user' OR `Seller`='$user')");
$f = 0;
while($row = $sqlz->fetch_assoc()) {
	$f++;
	if($f & 1) {
		$color = "lightgray";
	}else{
		$color = "darkgray";
	}
	if($row["Buyer"] == $user)
	{
		$info = "Bought";
	}
	else
	{
		$info = "Sold";
	}
	$mid = $db->real_escape_string($row["Market_Id"]);
	$getcn = $db->query("SELECT * FROM Wallets WHERE `Id`='$mid'");
	$row_ = $getcn->fetch_assoc();
	$cname = $row_['Acronymn']; /* $getcn */;
	$time = date("Y-m-d H:i:s", $row["Timestamp"]);
?>
<tr class="<?php echo $color; ?>">
	<td><?php echo $time; ?></td>
	<td><?php echo $cname; ?></td>
	<td><?php echo $info; ?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Price"]);?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Quantity"]);?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Quantity"] * $row["Price"]);?></td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>

<div class="top aligncenter">
	<?php echo lang("Your Deposit History"); ?>
</div>
<div class="box">
<table class="page data">
<thead>
<tr class="blue">
	<th><?php echo lang("Coin"); ?></th>
	<th><?php echo lang("Quantity"); ?></th>
	<th>Tx ID</th>
</tr>
</thead>
<tbody>
<?php
$sqlz = $db->query("SELECT * FROM deposits WHERE `Account`='$act_name'");
$h = 0;
while ($row = $sqlz->fetch_assoc()) {
$h++;
if($h & 1) {
	$color2 = "lightgray";
}else{
	$color2 = "darkgray";
}
?>
<tr class="<?php echo $color2; ?>">
	<td><?php echo $row["Coin"];?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Amount"]);?></td>
	<td title="<?php echo $row["Transaction_Id"];?>"><?php echo substr($row["Transaction_Id"], 0, 10); ?>...</td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>

<div class="top aligncenter">
<?php echo lang("Your Pending Withdrawals"); ?>
</div>
<div class="box">
<table class="page data">
<thead>
<tr class="blue">
	<th><?php echo lang("Coin"); ?></th>
	<th><?php echo lang("Quantity"); ?></th>	
	<th><?php echo lang("Address"); ?></th>	
</tr>
</thead>
<tbody>
<?php
$sqlza = $db->query("SELECT * FROM Withdraw_Requests WHERE `User_Id`='$user' AND Confirmed IN (-1, 0, 1)");
$l = 0;
while ($row = $sqlza->fetch_assoc()) {
$l++;
if($l & 1) {
	$color3 = "lightgray";
}else{
	$color3 = "darkgray";
}
?>
<tr class="<?php echo $color3; ?>">
	<td><?php echo $row["CoinAcronymn"];?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Amount"]);?></td>
	<td title="<?php echo $row["Address"];?>"><?php echo substr($row["Address"], 0, 10); ?>...</td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>

<div class="top">
<?php echo lang("Your Withdraw History"); ?>
</div>
<div class="box">
<table class="page data">
<thead>
<tr class="blue">
	<th><?php echo lang("Coin"); ?></th>
	<th><?php echo lang("Quantity"); ?></th>	
	<th><?php echo lang("Address"); ?></th>	
</tr>
</thead>
<tbody>
<?php
$sqlz = $db->query("SELECT * FROM Withdraw_History WHERE `User`='$user'");
$m = 0;
while ($row = $sqlz->fetch_assoc()) {
$m++;
if($m & 1) {
	$color4 = "lightgray";
}else{
	$color4 = "darkgray";
}
?>
<tr class="<?php echo $color4; ?>">
	<td><?php echo $row["Coin"];?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Amount"]);?></td>
	<td title="<?php echo $row["Address"];?>"><?php echo substr($row["Address"], 0, 10);?>...</td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>


</div>
