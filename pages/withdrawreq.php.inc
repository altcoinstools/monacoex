<?php
function generateRandomString($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, strlen($characters) - 1)];
  }
  return $randomString;
}

if (isWithdrawalDisabled()) {
  echo lang("WITHDRAW_DISABLED");
  if ($twitter_id) {
    echo ' See our <a href="https://twitter.com/'.$twitter_id.'">Twitter Account</a> for details.';
  }
} else {
  if (!isUserLoggedIn()) {
    echo 'invalid request : not logged in';
  } else {
    $account = $loggedInUser->display_username;
    $user_id = $loggedInUser->user_id;
    $idtw    = $db->real_escape_string(strip_tags(isset($_GET["id"])?(int)$_GET["id"]:0));

    $init = $db->query("SELECT * FROM Wallets where `Id`='$idtw'");
    $row_i = $init->fetch_assoc();
    $coinfull = $row_i['Name']; /* $init */;
    $coin = $row_i['Acronymn']; /* $init */;
    $feecost = $row_i['Fee']; /* $init */;
    $minwithdrawal = $row_i['Minimum_Withdrawal']; /* $init */;
    $total = $loggedInUser->getbalance($idtw);
    $maxwithdrawal = $total * (1.0 - $feecost);
    $token = getToken($user_id, getIP());

    if (!isset($_SESSION["Withdraw_Attempts"])) {
      $_SESSION["Withdraw_Attempts"] = 0;
    }

    if (isset($_POST["withdraw"]))  {
      if ($_SESSION["Withdraw_Attempts"] > 2) {
	$account = $db->real_escape_string(strip_tags($loggedInUser->display_username));
	$uagent = $db->real_escape_string(getuseragent()); //get user agent
	$ip = $db->real_escape_string(getIP()); //get user ip
	$date = $db->real_escape_string(gettime());
	$sql = @$db->query("INSERT INTO access_violations (username, ip, user_agent, time) VALUES ('$account', '$ip', '$uagent', '$date');");
	$captcha = md5($_POST["captcha"]);

	if ($captcha != $_SESSION['captcha']) {
	  $errors[] = lang("CAPTCHA_FAIL");
	}
      }
      if ($_SESSION["Withdraw_Attempts"] > 3) {
	$ip_address = $db->real_escape_string(getIP());
	$date2 = $db->real_escape_string(gettime());
	$db->query("INSERT INTO bantables_ip (ip, date) VALUES ( '$ip_address', '$date2');");
      }
      $errors = array();
      $successes = array();
      $userdetails = fetchUserDetails($account);
//      $password = trim($_POST["password"]);
//      $entered_pass = generateHash($password,$userdetails["Password"]);
      $error = false;
      $success = false;
      $postedToken = filter_input(INPUT_POST, 'token');
      if (!empty($postedToken)) {
	if (isTokenValid($postedToken)) {
	  $amount = 0;
	  $fee = 0;
//	  if ($entered_pass != $userdetails["Password"]) {
//	    $errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
//	    $error = true;
//	  } else
	  if ($_POST["amount"] == NULL) {
	    $errors[] = lang("INVALID_AMOUNT");
	    $error = true;
	  } else if (!validate_number($_POST["amount"])) {
	    $errors[] = lang("N_A_N");
	    $error = true;
	  } else {
	    $amount = $db->real_escape_string($_POST["amount"]);
	    $fees = $amount *  $feecost;
	    if ($amount < $minwithdrawal) {
	      $errors[] = "The minimum allotted withdrawal for ".$coinfull." is ".$minwithdrawal." coins";
	      $error = true;
	    } else if ($amount > $maxwithdrawal) {
	      $errors[] = "The maximum allotted withdrawal for ".$coinfull." is ".$maxwithdrawal." coins";
	      $error = true;
	    } else if ($amount + $fees > $total) {
	      $errors[] = lang("INS_FUNDS");
	      $error = true;
	    }
	  }

	  if ($error == false) {
	    $success = true;
	    $to = $db->real_escape_string(strip_tags($_POST["recipient"]));
	    $from = $db->real_escape_string(strip_tags($user_id));

	    $ckey = generateRandomString(30);
	    $todo3 = $db->query("INSERT INTO Withdraw_Requests (`Amount`,`Address`,`User_Id`,`Wallet_Id`,`Account`,`CoinAcronymn`,`Confirmation`, `Fee`) VALUES ('$amount','$to','$from','$idtw','$account','$coin','$ckey', '$fees')");
	    if ($todo3 == null) {
	      $db->error($todo3);
	      die();
	    } else {
	      TakeMoney($amount + $fees, $user_id, $idtw);
	      $successes[] = lang('WITHDRAW_PENDING_AVAILABLE');
	      successBlock($successes);

	      /* Recalculate total and maxwithdrawal as withdrawal is done. */
	      $total = $loggedInUser->getbalance($idtw);
	      $maxwithdrawal = $total * (1.0 - $feecost);

	      $userdetails = fetchUserDetails($account);
	      $mail = new userCakeMail();
	      $scheme = ($_SERVER['HTTPS']=='on') ? 'https://' : 'http://';
	      $confirm_url = lang("CONFIRM")."\n".$scheme.$_SERVER['HTTP_HOST']."/index.php?page=withdraw&confirm=".$ckey;
	      $deny_url = lang("DENY")."\n".$scheme.$_SERVER['HTTP_HOST']."/index.php?page=withdraw&deny=".$ckey;
	      $hooks = array(
		"searchStrs" => array("#CONFIRM-URL#","#DENY-URL#","#USERNAME#","#AMOUNT#","#COIN#"),
		"subjectStrs" => array($confirm_url,$deny_url,$userdetails["Username"],$amount,$coin)
	      );
	      if (!$mail->newTemplateMsg("confirm-withdrawl-request.txt",$hooks)) {
		$errors[] = lang("MAIL_TEMPLATE_BUILD_ERROR");
	      } else {
		if (!$mail->sendMail($userdetails["Email"],"Confirm Your Withdrawal")) {
		  die("Unable to send verifaction email! Please check your email on your account settings and try again!");
		} else {
		  echo lang('WITHDRAW_CONFIRMATION_SENT');
		}
	      }
	    }
     	  } else {
	    if (!isset($_SESSION["Withdraw_Attempts"])) {
	      $_SESSION["Withdraw_Attempts"] = 1;
	    } else {
	      $_SESSION["Withdraw_Attempts"]++;
	    }
	    errorBlock($errors);
	  }
	}
      } else {
	die(lang("FORGOTPASS_INVALID_TOKEN"));
      }
    }
?>
			<link rel="stylesheet" type="text/css" href="assets/css/register.css" />
			<script>
			 function fillwithdraw() {
			   $("#Amount").val(<?php echo sprintf("%.8f",$maxwithdrawal); ?>);
			   $("#withdraw_message").html("");
			 }

			 function check_amount() {
			   var amount = $("#Amount").val();
			   var fee = "";
			   if (isFinite(amount)) {
			     fee = amount * <?php echo $feecost; ?>;
			     var minwithdrawal = <?php echo $minwithdrawal; ?>;
			     var maxwithdrawal = <?php echo $maxwithdrawal; ?>;
			     if (amount > maxwithdrawal) {
			       $("#withdraw_message").html("The maximum allotted withdrawal is " + maxwithdrawal + " coins");
			     } else if (amount < minwithdrawal) {
			       $("#withdraw_message").html("The minimum allotted withdrawal is " + minwithdrawal + " coins");
			     } else {
			       $("#withdraw_message").html("");
			     }			      	
			     $("#fee").html(fee);
			   }
			   
			 }
			</script>
			<hr class="five">
			<h4 id="withdraw_message" style="color: red"></h4>
			<h2>Withdraw <?php echo $coinfull; ?></h2>
			<table border="0" cellpadding="0" ceppspacing="0" id="withdrawinfo">
				<tr>
					<th>Available balance</th>
					<td class="value"><?php echo sprintf("%.8f",$total); ?></td>
				</tr>
				<tr>
					<th>Maximum withdrawal</th>
					<td class="value" onclick="fillwithdraw();" style="cursor:pointer; text-decoration: underline;">
						<?php echo  sprintf("%.8f",$maxwithdrawal); ?>
					</td>
				</tr>
				<tr>
					<th>Minimum withdrawal</th>
					<td class="value"><?php echo sprintf("%.8f",$minwithdrawal); ?></td>
				</tr>
			</table>
			<hr class="five">
			<table border="0" cellpadding="0" cellspacing="0" id="withdrawform">
				<form name='withdraw' action='index.php?page=withdrawreq&id=<?php echo $idtw; ?>' method="POST" autocomplete="off">
					<input type="hidden" name="token" value="<?php echo $token;?>"/>
					<tr>
						<td><input id="Amount" name="amount" type="text" placeholder="Amount(<?php echo $feecost * 100; ?>% fee applies)" value="" class="field" onkeyup="check_amount()" pattern="\d+(\.\d{0,8})?" step="0.00000001" required /></td>
					</tr>
					<tr>
					  <td>Withdraw fee: <span id="fee"></span></td>
					</tr>
					<tr>
						<td><input name="recipient" type="text" placeholder="<?php echo lang("PLACEHOLDER_RECEIVING_ADDRESS"); ?>" value="" class="field" autocomplete="off"/></td>
					</tr>
<!--
					<tr>
						<td><input type="password" name="password" placeholder="<?php echo lang("PLACEHOLDER_PASSWORD"); ?>" value="" class="field" /></td>
					</tr>
-->
<?php if ($_SESSION["Withdraw_Attempts"] > 2) { ?>
					<tr>
						<td><img src="pages/docs/captcha.php" class="captcha aligncenter"></td>
					</tr>
					<tr>
						<td><input name="captcha" type="text" placeholder="<?php echo lang("PLACEHOLDER_ENTER_SECURITY_CODE"); ?>" class="field"></td>
					</tr>
<?php } ?>
					<tr>
						<td valign="top"><div class="aligncenter"><input id="submit" type="submit" value="<?php echo lang("WITHDRAW") ?>" name="withdraw" class="blues" /></div></td>
					</tr>
				</form>
			</table>
<?php
  }
}
?>
