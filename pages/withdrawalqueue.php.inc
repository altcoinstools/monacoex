<?php
require_once('models/withdraw_common.php');
if(!isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
if(!isUserAdmin($id)) {
	redirect_url('access_denied.php');
}
?>
<h1>Pending Withdrawals</h1>
<table class="page">
	<thead>
		<tr>
			<th style="width: 10%">User</td>
			<th style="width: 15%;">Amount</th>
			<th style="width: 10%;"><?php echo lang("Coin"); ?></th>
			<th style="width: 40%;">Recipient</th>	
			<th style="width: 25%;">Options</td>
		</tr>
	</thead>
	<tbody>
<?php
$getreq = $db->query("SELECT * FROM Withdraw_Requests WHERE Confirmed IN (-1, 0, 1) ORDER BY (id) ASC");
if($getreq->num_rows > 0) {
	while ($row = $getreq->fetch_assoc()) {
		$balance = $row["Amount"];
		$coinid = $row["CoinAcronymn"];
		$getcoin = $db->query("SELECT * FROM Wallets WHERE `id`='$coinid' OR `Acronymn`='$coinid'");
		$row_ = $getcoin->fetch_assoc();
		$coinacr = $row_['Acronymn']; /* $getcoin */;
		if($row["Confirmed"] == 1) {
			$appurl = "<a href=\"index.php?page=withdrawalqueue&approve=".$row["Id"]."\">Approve</a> | ";
		} else {
			$appurl = "";
		}
?>
		<tr>
			<td><?php echo $row["Account"]; ?></td>
			<td class="value"><?php echo sprintf("%.8f",$balance); ?></td>
			<td class="value"><?php echo $coinacr; ?></td>
			<td title="<?php echo $row["Address"]; ?>"><?php echo substr($row["Address"], 0, 10); ?>...</td>
			<td><?php echo $appurl; ?><a href="index.php?page=withdrawalqueue&cancel=<?php echo$row["Id"]; ?>">Cancel</a></td>
		</tr>
<?php
	}
} else {
?>
		<tr>
			<td style="width: 100%; text-align: center;" colspan="5">No Pending withdrawals</td>
		</tr>
<?php
}
?>
	</tbody>
</table>
<h1>Check User History</h1>
<form action="index.php?page=withdrawalqueue" method="POST" name="checkvalid">
<table>
	<tr>
		<td><input type="text" name="username" class="field"  placeholder="username" /></td>
		<td><input type="text" name="coinname" class="field" placeholder="coin" /></td>
		<td><input type="submit"  class="blues" value="check" name="checkvalid" /></td>
	</tr>
</table>
</form>
<?php
if(isset($_POST["checkvalid"])) {

	$user = $db->real_escape_string(strip_tags($_POST["username"]));
	$coin = $db->real_escape_string(strip_tags($_POST["coinname"]));
	
	if($coin == null) {
		$getu = $db->query("SELECT `User_ID` FROM `userCake_Users` WHERE `Username` = '$user'");
		$row_ = $getu->fetch_assoc();
		$u_id = $row_['User_ID']; /* $getu */;

		$getc = $db->query("SELECT `Wallet_ID` FROM `Wallets` WHERE `Acronymn` = '$coin'");
		$row_ = $getc->fetch_assoc();
		$wa_id = $row_['Wallet_ID']; /* $getc */;

		$depq = $db->query("SELECT * FROM `deposits` WHERE `Account`='$user' AND `Paid`='1'");
		$tradeq = $db->query("SELECT * FROM `Trade_History` WHERE `Buyer` = '$u_id' OR `Seller`='$u_id'");
		$ctrades = $db->query("SELECT * FROM `trades` WHERE `User_ID`='$u_id'");
		$canceled = $db->query("SELECT * FROM `Canceled_Trades` WHERE `User_ID`='$u_id'");
		$wdhist = $db->query("SELECT * FROM `Withdraw_History` WHERE `User`='$u_id'");
		$balq = $db->query("SELECT * FROM `balances` WHERE `User_ID`='$u_id'");
	}else{
		$getu = $db->query("SELECT `User_ID` FROM `userCake_Users` WHERE `Username` = '$user'");
		$row_ = $getu->fetch_assoc();
		$u_id = $row_['User_ID']; /* $getu */;

		$ctrades = $db->query("SELECT * FROM `trades` WHERE `User_ID`='$u_id' AND `Type`='$coin'");
		$canceled = $db->query("SELECT * FROM `Canceled_Trades` WHERE `User_ID`='$u_id' AND `Type`='$coin'");
		$wdhist = $db->query("SELECT * FROM `Withdraw_History` WHERE `User`='$u_id' AND `Coin`='$coin'");

		$getc = $db->query("SELECT `Wallet_ID` FROM `Wallets` WHERE `Acronymn` = '$coin'");
		$row_ = $getc->fetch_assoc();
		$wa_id = $row_['Wallet_ID']; /* $getc */;

		$depq = $db->query("SELECT * FROM `deposits` WHERE `Account`='$user' AND `Coin`='$coin' AND `Paid`='1'");
		$tradeq = $db->query("SELECT * FROM `Trade_History` WHERE `Buyer` = '$u_id' OR `Seller`='$u_id' AND `Market_Id`='$wa_id'");
		$balq = $db->query("SELECT * FROM `balances` WHERE `User_ID`='$u_id' AND `Wallet_ID`='$wa_id'");

	}
	
	echo '<h3>Balance, Deposit, and Transaction Information For '.$user.' :</h3>';
	echo '<table class="page">
	<tr>
			<td><div class="aligncenter">Deposits</div></td>
	</tr>
	';
	while($row = $depq->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Wallet : '.$row["Coin"].' | Amount : '.sprintf("%.8f",$row["Amount"]).' | Tx : '.$row["Transaction_Id"].'</td>
		</tr>
		';
	}
	echo'
	<tr>
		<td><div class="aligncenter">Pending Trades</div></td>
	</tr>
	';
	while($row = $ctrades->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Wallet : '.$row["Type"].' | Price: '.sprintf("%.8f",$row["Value"]).' | Quantity :'.$row["Amount"].'</td>
		</tr>
		';
	}
	echo'
	<tr>
		<td><div class="aligncenter">Canceled Trades</div></td>
	</tr>
	';
	while($row = $canceled->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Wallet : '.$row["Type"].' | Price: '.sprintf("%.8f",$row["Value"]).' | Quantity :'.$row["Amount"].'</td>
		</tr>
		';
	}
	echo'
	<tr>
		<td><div class="aligncenter">Trade History</div></td>
	</tr>
	';
	while($row = $tradeq->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Wallet : '.$row["Market_Id"].' | Price: '.sprintf("%.8f",$row["Price"]).' | Quantity :'.$row["Quantity"].'</td>
		</tr>
		';
	}
	echo'
	<tr>
		<td><div class="aligncenter">Balance</div></td>
	</tr>
	';
	while($row = $balq->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Wallet: '.$row["Wallet_ID"].' | Coin : '.$row["Coin"].' | Amount : '.sprintf("%.8f",$row["Amount"]).'</td>
		</tr>
		';
	}
	echo'
	<tr>
		<td><div class="aligncenter">Withdrawal History</div></td>
	</tr>
	';
	while($row = $wdhist->fetch_assoc()) {
		echo'
		
		<tr>
			<td>Coin : '.$row["Coin"].' | Amount : '.sprintf("%.8f",$row["Amount"]).' | Address : '.$row["Address"].'</td>
		</tr>
		';
	}
	echo '</table>';
}
if(isset($_GET["approve"])) {
	$request = $db->real_escape_string(strip_tags($_GET["approve"]));
	if (withdraw_approve($request)) {
		redirect_url('index.php?page=withdrawalqueue');
	}
}

if(isset($_GET["cancel"])) {
	$request = $db->real_escape_string(strip_tags($_GET["cancel"]));
	if (withdraw_cancel($request, true)) {
		redirect_url('index.php?page=withdrawalqueue');
	}
}
?>	
