<?php
if (isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
if(!isUserAdmin($id)) {
	redirect_url('access_denied.php');
}

if(isset($_GET["unban"])) {
	$ipis = $db->real_escape_string(strip_tags($_GET["unban"]));
	$sql1 = $db->query("DELETE FROM bantables_ip WHERE `ip`='$ipis'");
}
if(isset($_GET["ban"])) {
	$ipis = $db->real_escape_string(strip_tags($_GET["ban"]));
	$sql2 = $db->query("SELECT ip FROM bantables_ip WHERE ip = '$ipis';");
	$number_of_rows = $sql2->num_rows;

	if ($number_of_rows > 0) {

	}else {
		$time = $db->real_escape_string(gettime());
		$sqlxz = $db->query("INSERT INTO bantables_ip (ip, date) VALUES ( '$ipis', '$time');");
	}
}

?>

<table id="table_banned_ips">
	<thead>
		<tr>
			<th>Username</th>
			<th>IP</th>
			<th>User Agent</th>
			<th>Timestamp</th>
			<th>Ban IP</th>
		</tr>
	</thead>
	<tbody>
<?php
$access2 = $db->query("SELECT * FROM bantables_ip");
while($row = $access2->fetch_assoc()) {
?>
		<tr>
			<td>0</td>
			<td><?php echo $row["ip"]; ?></td>
			<td>0</td>
			<td>0</td>
			<td><a href="index.php?page=access_violations&unban=<?php echo $row["ip"]; ?>">UnBan</a></td>
		</tr>
<?php
}
?>
	</tbody>
</table>

<table id="table_access_violations">
	<thead>
		<tr>
			<th>Username</th>
			<th>IP</th>
			<th>User Agent</th>
			<th>Timestamp</th>
			<th>Ban IP</th>
		</tr>
	</thead>
	<tbody>
<?php
$access = $db->query("SELECT * FROM access_violations");
 while($row = $access->fetch_assoc()) {
?>
		<tr>
			<td><?php echo $row["username"] ?></td>
			<td><?php echo $row["ip"]; ?></td>
			<td><?php echo $row["user_agent"]; ?></td>
			<td><?php echo $row["time"]; ?></td>
			<td><a href="index.php?page=access_violations&ban=<?php echo $row["ip"]; ?>">Ban</a></td>
		</tr>
<?php } ?>
	</tbody>
</table>

