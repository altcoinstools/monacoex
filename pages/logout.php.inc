<?php
if(isUserLoggedIn()) {
	$loggedInUser->userLogOut();
	redirect_url('index.php?page=loggedout');
} else {
	redirect_url('index.php?page=home');
}
?>
