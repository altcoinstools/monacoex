<?php
/**~2014 MilanCoin Developers. All Rights Reserved.~*
 *Licensed Under the MIT License : http://www.opensource.org/licenses/mit-license.php
 *
 *WARRANTY INFORMATION:
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE. 
 ***************************************/
/* no need to login */
$getmarkets = $db->query("SELECT * FROM `Wallets` WHERE `disabled`='0' AND `Market_Id`!='0' ORDER BY `Acronymn` ASC");
$g = 0;
?>
<div class="container">
	<div class="innertube">
		<div>
			<hr class="five">
			<h1>Market Overview</h1>
			<hr class="five">
			<table id="overview_table" class="page">
			<thead>
				<tr class="blue">
					<td colspan="3" style="width: 20%;"><?php echo lang("MARKET"); ?></td>
					<td style="width: 20%; text-align: center; margin-right: 10px;"><?php echo lang("CURRENT_ASK"); ?></td>
					<td style="width: 20%; text-align: center; margin-right: 10px;"><?php echo lang("CURRENT_BID"); ?></td>
					<td style="width: 20%; text-align: center; margin-right: 10px;"><?php echo lang("LAST_PRICE"); ?></td>
					<td style="width: 20%; text-align: center; margin-right: 10px;"><?php echo lang("VOLUME"); ?></td>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="7" style="text-align: right;"><?php echo lang("UNIT"); ?> : MONA</td>
				</tr>
			</tfoot>
			<tbody>
<?php
while($row_g = $getmarkets->fetch_assoc()) {
	if ( $g++ & 1 ) {
		$color = "lightgray";
	} else {
		$color = "darkgray";
	}
	$market_id = $row_g['Id']; /* $getmarkets */;
	$name = $row_g['Name']; /* $getmarkets */;
	$acro = $row_g['Acronymn']; /* $getmarkets */;

	$sqltrades1 = $db->query("SELECT Price FROM `Trade_History` WHERE `Market_Id`='$market_id' ORDER BY Timestamp DESC LIMIT 1");
	$row_ = $sqltrades1->fetch_assoc();
	$last_trade = $row_['Price']; /* $sqltrades1 */;

	$sqltrades2 = $db->query("SELECT Value FROM `trades` WHERE `From`='$acro' ORDER BY `Value` ASC LIMIT 1");
	$row_ = $sqltrades2->fetch_assoc();
	$curask = $row_['Value']; /* $sqltrades2 */;

	$sqltrades3 = $db->query("SELECT Value FROM `trades` WHERE `To`='$acro' ORDER BY `Value` DESC LIMIT 1");
	$row_ = $sqltrades3->fetch_assoc();
	$curoffer = $row_['Value']; /* $sqltrades3 */;
	$timez = time() - (60*60*24);

	$sql24hour = $db->query("SELECT SUM(Quantity * Price) as Volume FROM `Trade_History` WHERE `Timestamp` >= '$timez' AND `Market_Id`='$market_id' LIMIT 1");
	$row_ = $sql24hour->fetch_assoc();
	$volume = $row_['Volume']; /* $sql24hour */
?>
					<tr id="market_id_<?php echo $market_id; ?>" style="cursor: pointer;" title="<?php echo $name; ?> Market" class="<?php echo $color; ?>" onclick="jump_to_market(<?php echo $market_id; ?>);">
						<td><?php echo $acro; ?></td>
						<td>/</td>
						<td>MONA</td>
						<td class="value"><?php echo sprintf("%.8f", $curask); ?></td>
						<td class="value"><?php echo sprintf("%.8f", $curoffer); ?></td>
						<td class="value"><?php echo sprintf("%.8f",$last_trade); ?></td>
						<td class="value"><?php echo sprintf("%.8f", $volume); ?></td>
					</tr>
<?php
}
?>
				</tbody>
			</table>
		</div>
	</div>
</div>
