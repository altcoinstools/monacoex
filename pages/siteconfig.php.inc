<?php
/**~2014 MilanCoin Developers. All Rights Reserved.~*
 *Licensed Under the MIT License : http://www.opensource.org/licenses/mit-license.php
 *
 *WARRANTY INFORMATION:
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE. 
 ***************************************/
if(!isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
if(!isUserAdmin($id)) {
	redirect_url('access_denied.php');
}

if(isset($_GET["s_id"])) {
	$confid = $db->real_escape_string($_GET["s_id"]);
	$action1 = $db->query("SELECT * FROM config WHERE `id`='$confid'");
	if($action1->num_rows <= 0) {
		die("error : ".$db->error($action1));
	}
	$row_ = $action1->fetch_assoc();
	$setting = $row_['setting']; /* $action1 */;
	if($setting == 1) {
		$outputted = 0;
	}else{
		$outputted = 1;
	}
	$action2 = $db->query("UPDATE `config` SET  `setting` = '$outputted' WHERE `id` = '$confid' ");
	redirect_url('index.php?page=siteconfig');
}

$siteconfig = $db->query("SELECT * FROM config");
?>
<h1>Site Configuration</h1>
<table class="page">
<?php
while($row = $siteconfig->fetch_assoc()){
	
	if($row['setting'] == 1){
		$status = "<font color='red'>Disabled</font>";
	}else{
		$status = "<font color='green'>Enabled</font>";
	}
echo'

	<tr>
		<td><h2>'.strtoupper($row["name"]).'</h2></td>
		<td><h3>Status: '.$status.'</h3></td>
		<td><a class="blues stdsize" href="index.php?page=siteconfig&s_id='.$row["id"].'" name="s_id" />Change Setting</a></td>
	</tr>
';
}

?>
</table>
