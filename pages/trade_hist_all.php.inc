<?php
$id     = $db->real_escape_string($_GET["market"]);
$result = $db->query("SELECT * FROM Wallets WHERE `Id`='$id'");
$raw_ = $result->fetch_assoc();
$name   = $db->real_escape_string($row_['Acronymn']); /* $result */;
$type = $name;
?>
<div class="top aligncenter">
	<?php echo $name; ?> Trade History(most recent first)
</div>
<div class="box">
<table class="page data">
<thead>
<tr>
	<th style="width: 25%;">Date</th>
	<th style="width: 25%;"><?php echo lang("Price"); ?></th>
	<th style="width: 25%;"><?php echo lang("Quantity"); ?>(<?php echo $name;?>)</th>
	<th style="width: 25%;">Total (MONA)</th>
</tr>
</thead>
<tbody>
<?php
$sqlz = $db->query("SELECT * FROM Trade_History WHERE `Market_Id`='$id' ORDER BY `Timestamp` DESC");
$g = 0;
while ($row = $sqlz->fetch_assoc()) {
$g++;
if($g & 1) {
	$color = "lightgray";
} else {
	$color = "darkgray";
}
$time = date("Y-m-d H:i:s", $row["Timestamp"]);
?>
<tr class="<?php echo $color; ?>">
	<td><?php echo $time;?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Price"]); ?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Quantity"]); ?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Quantity"] * $row["Price"]); ?></td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>
