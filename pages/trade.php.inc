<?php 
include("models/class.trade.php");
$id = $db->real_escape_string(strip_tags((int)$_GET["market"]));
if(!is_numeric($id)) {
	redirect_url('index.php?page=invalid_market');
}
$result = $db->query("SELECT * FROM Wallets WHERE `Id`='$id'");
$row_ = $result->fetch_assoc();
$name = $db->real_escape_string($row_['Acronymn']); /* $result */;
$fullname = $db->real_escape_string($row_['Name']); /* $result */;
$disabled = $db->real_escape_string($row_['disabled']); /* $result */;
$market_id = $row_['Market_Id']; /* $result */;

$errors = array();
$error = false;

if (isUserLoggedIn()) 
{ 
        $user_id = $loggedInUser->user_id;
	$successes = array();
	$success = false;
	$ip = getIP();
	$token1 = getToken($user_id,$ip);
	$token2 = getToken($user_id,$ip);
	$getbal = $db->query("SELECT SUM(Amount) as `Amount` FROM balances WHERE User_ID='$user_id' AND `Wallet_ID` = '$id'");
	$row_ = $getbal->fetch_assoc();
	$curbal = $row_['Amount']; /* $getbal */;
	if($curbal == null){
		$curbal = "0.00000000";
	}
	$getbal2 = $db->query("SELECT SUM(Amount) as `Amount` FROM balances WHERE User_ID='$user_id' AND `Wallet_ID` = '$market_id'");
	$row_ = $getbal2->fetch_assoc();
	$curbal2 = $row_['Amount']; /* $getbal2 */;
	if($curbal2 == null){
		$curbal2 = "0.00000000";
	}
}



if($id == 1) {
	redirect_url('index.php?page=account');
}
if($disabled == 1) {
	redirect_url('index.php?page=invalid_market');
}
if($name == NULL) {
	die();
}
$SQL2 = $db->query("SELECT * FROM Wallets WHERE `Id`='$market_id'");
$row_ = $SQL2->fetch_assoc();
$Currency_1a = $row_['Acronymn']; /* $SQL2 */;
$Currency_1 = $row_['Id']; /* $SQL2 */;
$feecost = $row_['Fee']; /* $SQL2 */;
if(isset($_POST["price2"])) {
	//Don't touch unless authorized!
	if (!(validate_number($_POST["price2"]) &&
	      validate_number($_POST["Amount2"]))) {
		$error = true;
		$errors[] = "Invalid numeric format.<br/>";
	} else if ($_POST["price2"] > 0.000000009 && $_POST["Amount2"] > 0.000000009) {
		$postedToken = filter_input(INPUT_POST, 'token2');
		if(!empty($postedToken)) {
			if(isTokenValid($postedToken)) {
				$PricePer = $db->real_escape_string($_POST["price2"]);
				$Amount = $db->real_escape_string($_POST["Amount2"]);
				$X = sprintf("%.8f", $Amount / $PricePer);
				$Fees = sprintf("%.8f", $Amount * $feecost);
				$Total = sprintf("%.8f", $Amount + $Fees);
				if(TakeMoney($Total,$user_id,$Currency_1) == true) {
					$New_Trade = new Trade();
					$New_Trade->trade_to = $name;
					$New_Trade->trade_from = $Currency_1a;
					$New_Trade->trade_amount = $X;
					$New_Trade->trade_value = $PricePer;
					$New_Trade->trade_owner = $user_id;
					$New_Trade->trade_type = $name;
					$New_Trade->trade_fees = $Fees;
					$New_Trade->trade_total = $Total;
					$New_Trade->trade_type = $name;
					$New_Trade->standard = $Amount;
					$New_Trade->GetEquivalentTrade();
					$New_Trade->ExecuteTrade();
					redirect_url('index.php?page=trade&market='.$id);
				}else{
					$error = true;
					$errors[] = "You cannot afford that!<br/>";
				}
			}else{
				echo "invalid token";
				die();
			}
		}
	}else{
		$error = true;
		$errors[] = "Please fill in all the forms!!<br/>";
	}
}
function completecancel($ids,$id) {
	global $db;

	$cord = $db->query("INSERT INTO Canceled_Trades SELECT * FROM trades WHERE `Id`='$ids'");
	$corq = $db->query("DELETE FROM trades WHERE `Id`='$ids'");
	if (!$corq) {
	}else{
		redirect_url('index.php?page=trade&market='.$id);
	}
}
/*the idea is to reload the page since completed orders don't update immediately on the trade page.*/
if(isset($_GET["trade"])) {
	if(isset($_GET["clickedcancel"])) {
		$clickedcancel = $db->real_escape_string($_GET["clickedcancel"]);
		if($clickedcancel == "true") {
			$ids      = $db->real_escape_string($_GET["trade"]);
			$tradesql = $db->query("SELECT * FROM trades WHERE `Id`='$ids'");
			$row_ = $tradesql->fetch_assoc();
			$from     = $row_['From']; /* $tradesql */;
			$owner    = $row_['User_ID']; /* $tradesql */;
			$type     = $row_['Type']; /* $tradesql */;
			$o_fee    = $row_['Fee']; /* $tradesql */;
			$Amount   = $row_['Amount']; /* $tradesql */;
			$Price    = $row_['Value']; /* $tradesql */;
			$Total    = $row_['Total']; /* $tradesql */;

			$sql      = $db->query("SELECT * FROM Wallets WHERE `Acronymn`='$from'");
			$row_ = $sql->fetch_assoc();
			$from_id  = $row_['Id']; /* $sql */;

			if($owner == $user_id || isUserAdmin($user_id)) {
				if($from != $type) {
					AddMoney($Total,$owner,$from_id);
					if(isset($id)) {
						completecancel($ids,$id);
					}
				} else {
					AddMoney($Amount,$owner,$from_id);
					if(isset($id)) {
						completecancel($ids,$id);
					}
				}
			}
		}
	}
}

if (isset($_GET["cancel"])) {
	/*this is a more secure method of canceling. double checks to make sure the user hasn't completed the trade.*/
	$ids = $db->real_escape_string($_GET["cancel"]);
	redirect_url('index.php?page=trade&market='.$id.'&trade='.$ids.'&clickedcancel=true');
}


//--------------------------------------
if(isset($_POST["Amount"]))
{
//Don't touch unless authorized!
	if (!(validate_number($_POST["price1"]) &&
	      validate_number($_POST["Amount"]))) {
		$error = true;
		$errors[] = "Invalid numeric format.<br/>";
	} else if ($_POST["price1"] > 0.000000009 && $_POST["Amount"] > 0.000000009) {
		$postedToken = filter_input(INPUT_POST, 'token1');
		if(!empty($postedToken)){
			if(isTokenValid($postedToken)){
				$PricePer = $db->real_escape_string($_POST["price1"]);
				$Amount = $db->real_escape_string($_POST["Amount"]);
                                $Fees = sprintf("%.8f", $Amount * $feecost);
                                $Total = sprintf("%.8f", $Amount);
				if(TakeMoney($Amount,$user_id,$id) == true) {
					$New_Trade = new Trade();
					$New_Trade->trade_to = $Currency_1a;
					$New_Trade->trade_from = $name;
					$New_Trade->trade_amount = $Amount;
					$New_Trade->trade_value = $PricePer;
					$New_Trade->trade_owner = $user_id;
					$New_Trade->trade_type = $name;
					$New_Trade->trade_fees = $Fees;
					$New_Trade->trade_total = $Total;
					$New_Trade->trade_type = $name;
					$New_Trade->standard = $Amount;
					$New_Trade->GetEquivalentTrade();
					$New_Trade->ExecuteTrade();
					redirect_url('index.php?page=trade&market='.$id.'');
				}
				else
				{
					$error = true;
					$errors[] = "You cannot afford that!</br>";
				}
				
			}else{
				$error = true;
				$errors[] = "Invalid Token.";
			}
		}	
	}else{
		$error = true;
		$errors[] = "Please fill in all the forms!!<br/>";
		
	}
}

if($error == false) {
	
}else{
	errorBlock($errors);
}
?>
<div class="tabber">
	<div class="tabbertab" title="Trade <?php echo $fullname; ?>">
		<div id="boxB">
			<div id="boxA">
				<div id="col1">
					<!-- Sell Form-->
					<?php if(isUserLoggedIn())
					{
					?>
					<div id="sellform">
						<h3 style="color:#000; text-align: center;">Sell: Avail. <?php echo $name; ?>: <span style="cursor:pointer; " id="secondaryBalance"><u><?php echo sprintf("%.8f",$curbal); ?></u></span></h3>
						<form action="index.php?page=trade&market=<?php echo $id; ?>" method="POST" autocomplete="off" history="off" onsubmit="document.getElementById('#Sell').disabled = 1;"> 
							<input type="hidden" name="token1" value="<?php echo $token1;?>"/>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="number" style="width: 100%; border: solid 1px #666666;" name="price1" onBlur="calculateFees1(this)" id="price1" placeholder="Price(MONA)" pattern="\d+(\.\d{0,8})?" step="0.00000001" required/>
							</div>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="number" style="width: 100%; border: solid 1px #666666;" name="Amount" onBlur="calculateFees1(this)" id="Amount" placeholder="Amount(<?php echo $name; ?>)" pattern="\d+(\.\d{0,8})?" step="0.00000001" required/>
							</div>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="text" style="width: 100%;" id="earn1" placeholder="Receive(MONA)" tabindex="-1" onFocus="this.form.elements['Sell'].focus()" readonly />
							</div>
							<div class="clearfix">
								<input class="miniblues" type="submit" name="Sell" value="Sell" id="Sell" onclick="this.disabled=true;this.value='Submitting trade...';this.form.submit();"/>
							</div>
						</form>
					</div>
					<?php } ?>
					<!--Sell Order Book-->
					<div class="top" style="text-align: center;">
						Sell Orders
					</div>
					<div class="box">
						<table class="page" style="width: 100%;">
							<thead>
								<tr>
									<th style="width: 33%;"><?php echo lang("Price"); ?></th>
									<th style="width: 33%;"><?php echo lang("Quantity"); ?></th>
									<th style="width: 33%;">Total</th>
								</tr>
							</thead>
							<tbody id="sellorders">
							</tbody>
						</table>
					</div>
				</div>
				<div id="col2">
					<!--Buy Form-->
					<?php if (isUserLoggedIn()) { ?>
					<div id="buyform">
						<h3 style="color:#000; text-align: center;">Buy: Avail. MONA: <span style="cursor:pointer; " id="btcBalance"><u><?php echo sprintf("%.8f",$curbal2); ?></u></span></h3>
						<form action="index.php?page=trade&market=<?php echo $id; ?>" method="POST" autocomplete="off" history="off" onsubmit="document.getElementById('#Buy').disabled = 1;">
							<input type="hidden" name="token2" value="<?php echo $token2;?>"/>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="number" style="width: 100%; border: solid 1px #666666;" name="price2" onBlur="calculateFees2(this)" id="price2" placeholder="Price(MONA)" pattern="\d+(\.\d{0,8})?" step="0.00000001" required />
							</div>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="text" style="width: 100%;" id="fee2" placeholder="Receive (<?php echo $name;?>)" tabindex="-1" onFocus="this.form.elements['Amount2'].focus()" readonly />
							</div>
							<div style="min-width: 100px; max-width: 30%; padding-left: 1%; padding-right: 1%; float: left;">
								<input class="fieldsmall" type="number" style="width: 100%; border: solid 1px #666666;" name="Amount2" onBlur="calculateFees2(this)" id="Amount2" placeholder="Amount(MONA)" pattern="\d+(\.\d{0,8})?" step="0.00000001" required />
							</div>
							<div class="clearfix">
								<input class="miniblues" type="submit" name="Buy" id="Buy" value="Buy" onclick="this.disabled=true;this.value='Submitting trade...';this.form.submit();"/>
							</div>
						</form>
					</div>
					<?php  } ?>
					
					<!--Buy Order Book-->
					<div class="top" style="text-align: center;">
						Buy Orders
					</div>
					<div class="box">
						<table class="page" style="width: 100%;">
							<thead>
								<tr>
									<th style="width: 33%;"><?php echo lang("Price"); ?></th>
									<th style="width: 33%;"><?php echo lang("Quantity"); ?></th>
									<th style="width: 33%;">Total</th>
								</tr>
							</thead>
							<tbody id="buyorders">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
		<?php if (isUserLoggedIn()) { ?>
		<div id="user-orders" class="clearfix">
		<?php include("open_orders.php"); ?>
		</div>
		<div id="personal-history">
			<?php include("trade_history.php.inc");?>
		</div>
		<?php }  ?>
	</div>
	
	<!--div class="tabbertab" id="charttab" title="View Charts">
		<div id="chart">
			<button class="toggle" id="chartshow" onclick="this.disabled=true;this.value='Fetching Trade Data..';">Load Data</button>
			<canvas id='canvas1' width='0px' height='0px'></canvas>
		</div>
	</div-->
	<div class="tabbertab" title="Market History">
		<div id="user-orders">
			<div id="all-history">
					<?php include("trade_hist_all.php.inc");?>
			</div>
		</div>
	</div>
</div>

