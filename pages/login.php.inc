<style type="text/css">
#chat_toggle {
	visibility: hidden;
	display: none;
}
</style>
<?php
if (!isset($_SESSION["Login_Attempts"])){$_SESSION["Login_Attempts"]=0;}
if(isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
if(isLoginDisabled()) {
    echo lang("LOGIN_DISABLED");
}else{
	if(!empty($_POST)) {
		if($_SESSION["Login_Attempts"] > 2)
		{
			$account = $db->real_escape_string(strip_tags($loggedInUser->display_username));
			$uagent = $db->real_escape_string(getuseragent()); //get user agent
			$ip = $db->real_escape_string(getIP()); //get user ip
			$account = $db->real_escape_string("Guest/Not Logged In");
			$date = $db->real_escape_string(gettime());
			$sql = @$db->query("INSERT INTO access_violations (username, ip, user_agent, time) VALUES ('$account', '$ip', '$uagent', '$date');");
			$captcha = md5($_POST["captcha"]);
			
			if ($captcha != $_SESSION['captcha'])
			{
				$errors[] = lang("CAPTCHA_FAIL");
			}
			
		}
		if($_SESSION["Login_Attempts"] > 3) {
						$ip_address = $db->real_escape_string(getIP());
						$date2 = $db->real_escape_string(gettime());
						$db->query("INSERT INTO bantables_ip (ip, date) VALUES ( '$ip_address', '$date2');");	
		}
			$errors = array();

			$username = trim($_POST["username"]);

			$password = trim($_POST["password"]);
			if($username == "") {
				$errors[] = lang("ACCOUNT_SPECIFY_USERNAME");
			}
			if($password == "") {
				$errors[] = lang("ACCOUNT_SPECIFY_PASSWORD");
			}
			if(count($errors) == 0)
			{

				if(!usernameExists($username))
				{
					$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
				}
				else
				{
					$userdetails = fetchUserDetails($username);
				
					if($userdetails["Banned"]==1)
					{
						$errors[] = "<p class='notify-red'>This account is banned!</p>";
					}
					else
					{
						$entered_pass = generateHash($password,$userdetails["Password"]);

						if($entered_pass != $userdetails["Password"])
						{
							$errors[] = lang("ACCOUNT_USER_OR_PASS_INVALID");
						}
						else
						{
							$loggedInUser = new loggedInUser();
							$loggedInUser->email = $userdetails["Email"];
							$loggedInUser->user_id = $userdetails["User_ID"];
							$loggedInUser->hash_pw = $userdetails["Password"];
							$loggedInUser->display_username = $userdetails["Username"];
							$loggedInUser->clean_username = $userdetails["Username_Clean"];		
							$loggedInUser->updateLastSignIn();
							$_SESSION["userCakeUser"] = $loggedInUser;
							redirect_url('index.php?page=account');
						}
					}
				}
			}
			if(count($errors) > 0) {
				if(!isset($_SESSION["Login_Attempts"]))
				{
					$_SESSION["Login_Attempts"] = 1;
				}else{
					$_SESSION["Login_Attempts"]++;
				}
				errorBlock($errors); 
		} 
	}
?> 
<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="assets/css/register.css" />
</head>
<div id="login-holder">

	<div id="logo-login">
		<h1><?php echo lang("LOGIN_TITLE"); ?></h1>
	</div>

	<div class="clear"></div>

	<h3>OAuth Login with...</h3>
	<ul>
		<li><a href="/auth/google">Google+</a></li>
		<li><a href="/auth/facebook">Facebook</a></li>
		<li><a href="/auth/yahoojp">Yahoo! Japan</a></li>
		<li><a href="/auth/github">GitHub</a></li>
	</ul>

	<div class="clear"></div>

	<div id="loginbox">

	<div id="login-inner">

	<form method="POST" action="index.php?page=login" autocomplete="off" onsubmit="document.getElementById('#loginbutton').disabled = 1;">

		<table border="0" cellpadding="0" cellspacing="0">

		<tr>
			<td><input name="username" type="text"  class="field stdsize" placeholder="<?php echo lang("PLACEHOLDER_USERNAME"); ?>" /></td>
			<br/>
		</tr>
		
		<tr>
			<td><input type="password" name="password" placeholder="<?php echo lang("PLACEHOLDER_PASSWORD"); ?>" class="field stdsize" /></td>
			<br/>
		</tr>
		<tr>
			<td><input type="checkbox" style="float: left; margin-right: 0.4em;" id="login-check" name="remember"/><label for="remember" style=" font-size: 18px; text-align: left; display: inline;"><?php echo lang("LOGIN_REMEMBER_ME"); ?></label></td>
			<hr class='five'>
			<br/>
			<br/>
		</tr>
		<tr>
<?php
	if($_SESSION["Login_Attempts"] > 2)
	{
		echo 
		'
		<tr>
			<td>
				<div class="aligncenter"><img src="pages/docs/captcha.php" class="captcha stdsize"></div>
			</td>
		</tr>
		<tr>
			<td>
				<input name="captcha" type="text" placeholder="<?php echo lang("PLACEHOLDER_ENTER_SECURITY_CODE"); ?>" class="field stdsize">
			</td>
		</tr>
		';
	}
?>
			<td><input type="submit" id="loginbutton" class="blues stdsize" onclick="this.disabled=true;this.value='<?php echo lang("LOGGING_IN"); ?>';this.form.submit();"/></td>
		</tr>
		<tr>
			<th></th>
		</tr>
		</table>	</div>
	<div class="clear"></div>
 </div>

	<div id="forgotbox">
		<div id="forgotbox-text"><a href="index.php?page=reset"><?php echo lang("LOGIN_FORGOT_PASSWORD"); ?></a></div>
	</div>
        <div>
		<a href="index.php?page=register"><?php echo lang("Register"); ?></a>
        </div>
</div>
</html>
<?php
}
?>
