<?php
include("../models/config.php");

$id     = $db->real_escape_string($_GET["market"]);
$sql = $db->query("SELECT t.Value AS Value, SUM(t.Amount) AS Amount, Value * Amount AS Total FROM trades AS t INNER JOIN Wallets AS w ON t.`From` = 'MONA' AND t.`Type`= w.Acronymn AND w.Id = '$id' GROUP BY t.Value ORDER BY t.Value DESC");
$g = 0;
while ($row = $sql->fetch_assoc()) {
	$g++;
	if($g & 1) {
		$color = "lightgray";
	} else {
		$color = "darkgray";
	}
	?>
		<tr style="cursor:pointer;" title="Click To Fill Order Form" class="<?php echo $color; ?>" onclick="document.getElementById('Amount').value = '<?php echo sprintf('%.8f',$row["Amount"]); ?>'; document.getElementById('price1').value = '<?php echo $row["Value"]; ?>';calculateFees1(this); ">
			<td class="value"><?php echo sprintf("%.8f", $row["Value"]); ?></td>
			<td class="value"><?php echo sprintf("%.8f", $row["Amount"]); ?></td>
			<td class="value"><?php echo sprintf("%.8f", $row["Total"]); ?></td>
		</tr>
<?php
}
?>
