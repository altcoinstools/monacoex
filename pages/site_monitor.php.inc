<?php
/**~2014 milancoin Developers. All Rights Reserved.~*
 *Licensed Under the MIT License : http://www.opensource.org/licenses/mit-license.php
 *
 *WARRANTY INFORMATION:
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE. 
 ***************************************/
if(!isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
if(!isUserAdmin($id)) {
	redirect_url('access_denied.php');
}
?>
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<h1>Site Monitor</h1>
<div class="serverload">
	<form action="" name="serverload" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="serverload" value="Show Server Load" class="blues stdsize" id="submit" />
	</form>	
</div>
<hr class="five" />
<div class="mysqlload">
	<form action="" name="mysqlload" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="mysqlload" value="Show SQL Status" class="blues stdsize" id="submit" />
	</form>
</div>
<hr class="five" />
<div class="users">
	<form action="" name="userstats" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="userstats" value="Show User Statistics" class="blues stdsize" id="submit" />
	</form>
</div>
<hr class="five" />
<div class="coinstatus">
	<form action="" name="coinstatus" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="coinstatus" value="Check Coin Stats" class="blues stdsize" id="submit" />
	</form>
</div>
<hr class="five" />
<div class="optsql">
	<form action="" name="optimizesql" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="optimizesql" value="Optimize SQL" class="blues stdsize" id="submit" />
	</form>
</div>
<hr class="five" />
<div class="earnings">
	<form action="" name="revenue" method="POST" onsubmit="document.getElementById('#submit').disabled = 1;">
		<input type="submit" name="revenue" value="Site Revenue" class="blues stdsize" id="submit" />
	</form>
</div>
<hr class="five" />
<div class="results" id="result">
<?php
if (isset($_POST["serverload"])) {
	echo '<h2>Result:</h2><br/>';
?>
		<a href class="miniblues" onclick="$('#result').html('');" height="30" width="200"/>Click Here To Close</a>
<?php
	echo'<b class="title-point">Server Load(main):</b></br>
		<table class="page">';
	
		$var_load = sys_getloadavg();
		
		foreach($var_load as $key => $value) {
			echo "<tr><td>".$key." : ".$value."</tr><td>";
		}
	echo'</table>';			

}

if (isset($_POST["mysqlload"])) {
	echo '<h2>Result:</h2><br/>';
?>
		<a href class="miniblues" onclick="$('#result').html('');" height="30" width="200"/>Click Here To Close</a>
<?php
		echo'<b class="title-point">MySQL Load:</b></br>
		<table class="page">';
			$mysqlload = explode("  ", $db->stat());
				foreach ($mysqlload as $key => $value){
					echo "<tr><td>".$key." : ".$value."<td></tr>";
				}
		echo'</table>';

}

if (isset($_POST["userstats"])) {
	echo '<h2>Result:</h2><br/>';
?>
		<a href class="miniblues" onclick="$('#result').html('');" height="30" width="200"/>Click Here To Close</a>
<?php
	echo '
	<b class="title-point">Users Online</b></br>
		<table class="page">';
			
			$getusers = $db->query("SELECT * FROM usersactive WHERE `id`=1");
			$row_ = $getusers->fetch_assoc();
			$total = $row_['total_users']; /* $getusers */;
			$upda = $row_['last_update']; /* $getusers */;

			$getloggedin = $db->query("SELECT COUNT(*) AS `count` FROM `userCake_Users` WHERE LastTimeSeen > DATE_SUB(NOW(), INTERVAL 5 MINUTE)");
			$row_ = $getloggedin->fetch_assoc();
			$loggedin = $row_['count']; /* $getloggedin */;
				echo "
				<tr>
					<td>Logged In : ".$loggedin."<td>
				</tr>
				<tr>
					<td>Total Users: ".$total."<td>
				</tr>
				<tr>
					<td>Last Updated : ".$upda."<td>
				</tr>
				
			
			
		</table>";	
}

if (isset($_POST["coinstatus"])) {
	echo '<h2>Result:</h2><br/>';
?>
		<a href class="miniblues" onclick="$('#result').html('');" height="30" width="200"/>Click Here To Close</a>
<?php
	echo'<b class="title-point">Coin Status:</b></br>
		<table class="page">
				<tr>
					<th>Name</th>
					<th>Balance</th>
					<th>Blocks</th>
					<th>Connections</th>
				</tr>
				';
				$result = $db->query("SELECT * FROM `Wallets` WHERE `disabled`='0' ORDER BY `Wallets`.`Acronymn` ASC");
				$g = 0;
				while ($row_ = $result->fetch_assoc()) {
					if($g++ & 1) {
						$color = "lightgray";
					} else {
						$color = "darkgray";
					}
					$id         = $row_['Id']; /* $result */;
					$pw         = $row_['Wallet_Password']; /* $result */;
					$usr        = $row_['Wallet_Username']; /* $result */;
					$acro       = $row_['Acronymn']; /* $result */;
					$wallet     = new Wallet($id);
					$id_check   = $loggedInUser->user_id;
					$requestkey = md5( hash('sha512', $id_check.$id.$usr.$pw));
					$info       = $wallet->GetInfo($id,$pw,$usr,$requestkey,$id_check);
					$balance    = round($info["balance"], 8);
					$blocks     = $info["blocks"];
					$connects   = isset($info["connections"])?$info["connections"]:1;
					echo
					'
					<tr class='.$color.'>
					<td>'.$acro.'</td>
					<td class="value">'.sprintf("%.8f", $balance).'</td>
					<td class="value">'.$blocks.'</td>
					<td class="value">'.$connects.'</td>
					</tr>
					';
				}			
		echo'</table></div>';
}
if(isset($_POST["optimizesql"])) {
	$db->query("OPTIMIZE TABLE TicketReplies");
	$db->query("OPTIMIZE TABLE Trade_History");
	$db->query("OPTIMIZE TABLE Wallets");
	$db->query("OPTIMIZE TABLE Withdraw_History");
	$db->query("OPTIMIZE TABLE Withdraw_Requests");
	$db->query("OPTIMIZE TABLE access_violations");
	$db->query("OPTIMIZE TABLE balances");
	$db->query("OPTIMIZE TABLE bantables_ip");
	$db->query("OPTIMIZE TABLE config");
	$db->query("OPTIMIZE TABLE deposits");
	$db->query("OPTIMIZE TABLE messages");
	$db->query("OPTIMIZE TABLE trades");
	$db->query("OPTIMIZE TABLE userCake_Groups");
	$db->query("OPTIMIZE TABLE userCake_Users");
	$db->query("OPTIMIZE TABLE usersactive");
echo "done : database was optimized";
}

if(isset($_POST["revenue"])) {
?>
		<a href class="miniblues" onclick="$('#result').html('');" height="30" width="200"/>Click Here To Close</a>
<?php

	$getearnings = $db->query("SELECT SUM(Amount) as `Amount`  FROM balances WHERE User_ID='-12' AND `Wallet_ID` = '1'");
	$row_ = $getearnings->fetch_assoc();
	$earnings = $row_['Amount']; /* $getearnings */;
	$earned = array();
	$earned["staff"]   = $earnings * 0.6;
	$earned["shares"] = $earnings * 0.2;
	$earned["site"]   = $earnings * 0.2;
	echo'<b class="title-point">Site Revenue:</b></br>
	<table class="page">';
	echo '<tr><td>Total</td><td>'.$earnings.'</td></tr>';
	foreach($earned as $key => $value) {
	
		echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
	
	}
	echo '</table>';
}
?>
</div>
