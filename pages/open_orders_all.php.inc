<?php
if(!isUserLoggedIn()) {
	redirect_url('index.php?page=login');
}
?>
<table class="page data">
<thead>
    <tr>
	<th style="width: 15%;"><?php echo lang("Coin"); ?></th>
	<th style="width: 15%;">Type</th>
	<th style="width: 20%;"><?php echo lang("Price"); ?> (MONA)</th>
	<th style="width: 20%;"><?php echo lang("Quantity") ?></th>
	<th style="width: 20%;">Total (MONA)</th>
    </tr>
  </thead>
  <tbody>
<?php
$user = $loggedInUser->user_id;
$sql = $db->query("SELECT `Id`, `From`, `Type`, `Value`, `Amount`, `Amount` * `Value` AS Total FROM trades WHERE `User_ID`='$user' ORDER BY `Value` ASC");

while ($row = $sql->fetch_assoc()) {
	$ids = $row["Id"];
	$from = $row["From"];
	$currency = $row["Type"];
	if($from == $row["Type"])
	{
		$type = "Sell";
	}
	else
	{
		$type = "Buy";
	}
?>
    <tr>
	<td><?php echo $currency; ?></td>
	<td><?php echo $type;?></td>
	<td class="value"><?php echo sprintf('%.8f', $row["Value"]);?></td>
	<td class="value"><?php echo sprintf('%.8f', $row["Amount"]);?></td>
	<td class="value"><?php echo sprintf('%.8f', $row["Total"]);?></td>
    </tr>
<?php
}
?>
  </tbody>
</table>
