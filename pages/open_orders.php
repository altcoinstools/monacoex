<?php
if(!isUserLoggedIn()) 
{
	redirect_url('index.php?page=login');
}

$id     = $db->real_escape_string($_GET["market"]);
$result = $db->query("SELECT * FROM Wallets WHERE `Id`=$id");
$row_ = $result->fetch_assoc();
$name   = $db->real_escape_string($row_['Acronymn']); /* $result */;
$type = $name;
$user = $loggedInUser->user_id;
$marketid = $_GET["market"];

$sql = $db->query("SELECT `Id`, `From`, `Type`, `Value`, `Amount`, `Value` * `Amount` AS `Total` FROM trades WHERE `Type`='$type' AND `User_ID`='$user' ORDER BY `Value` ASC");
?>
<div class="top" style="text-align: center;">
	Your Orders
</div>
<div class="box">
	<table class="page data">
	<thead>
	<tr>
		<th style="width: 20%;">Type</th>
		<th style="width: 20%;"><?php echo lang("Price"); ?> (MONA)</th>
		<th style="width: 20%;"><?php echo lang("Quantity"); ?> (<?php echo $name;?>)</th>
		<th style="width: 20%;">Total (MONA)</th>
		<th style="width: 20%;">Options</th>
	</tr>
	</thead>
	<tbody>
<?php
	$g = 0;
	while ($row = $sql->fetch_assoc()) {
		$g++;
		if($g & 1) {
			$color = "lightgray";
		} else {
			$color = "darkgray";
		}
		$ids = $row["Id"];
		$from = $row["From"];
		if($from == $row["Type"]) {
			$type = "Sell";
		} else {
			$type = "Buy";
		}
?>
		<tr class="<?php echo $color; ?>">
			<td><?php echo $type;?></td>
			<td class="value"><?php echo sprintf('%.8f', $row["Value"]);?></td>
			<td class="value"><?php echo sprintf('%.8f', $row["Amount"]);?></td>
			<td class="value"><?php echo sprintf('%.8f', $row["Total"]);?></td>
			<td class="value"><a href="index.php?page=trade&market=<?php echo $marketid; ?>&cancel=<?php echo $ids; ?>">Cancel</a></td>
		</tr>
<?php
	}
?>
	</tbody>
	</table>
</div>
