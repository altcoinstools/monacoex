<?php
/**~2014 milancoin Developers. All Rights Reserved.~*
 *Licensed Under the MIT License : http://www.opensource.org/licenses/mit-license.php
 *    +++++++++++++++++++++++
 *    +WARRANTY INFORMATION:+
 *    +++++++++++++++++++++++
 *THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *THE SOFTWARE. 
 ***************************************/
/***\/
 */	
if (isset($_GET['fchk'])) {
   /* for backward compatibility */
   redirect_url('index.php?page=update_balance&fchk='.$_GET['fchk']);
}
	if(!isUserLoggedIn()) {
		redirect_url('index.php?page=login');
	}
/***\/
 */     $user_id = addslashes(strip_tags($loggedInUser->user_id));
    $account = addslashes(strip_tags($loggedInUser->display_username));
/***\/
 */
	if(isUserAdmin($user_id)) {
		$sql = @$db->query("SELECT SUM(Amount) as `Amount`  FROM balances WHERE User_ID='-12' AND `Wallet_ID` = '1'");
		$row_ = $sql->fetch_assoc();
		echo'<h3>Welcome Admin the current fee earnings are: '.$row_['Amount'].' MONA</h3>';
	}
?>
<div class="tabber">
	<div class="tabbertab" title="<?php echo lang("ACCOUNT_MENU_BALANCES"); ?>">
		<?php
		/***\/
		 */	
			echo
			'
			<div style="width: 95%;">
			<table class="page">
			<thead>
			<tr>
				<th style="text-align: center;">'.lang("ACCOUNT_CURRENCY").'</th>
				<th style="text-align: center;">'.lang("ACCOUNT_AVAILABLE").'</th>
				<th style="text-align: center;">'.lang("ACCOUNT_PENDING").'</th>
				<th style="text-align: center;">'.lang("ACCOUNT_DEPOSIT").'</th>
				<th style="text-align: center;">'.lang("ACCOUNT_WITHDRAW").'</th>
			</tr>
			</thead>
			</tbody>';

			$user_id =  $loggedInUser->user_id;
			$sql = $db->query("SELECT * FROM Wallets WHERE `disabled`='0' ORDER BY `Id` ASC");
			$g = 0;
			while ($row = $sql->fetch_assoc()) {
					$g++;
					if($g & 1) {
						$color = "lightgray";
					} else {
						$color = "darkgray";
					}
					$coin = $row["Id"];
					$amount = 0;
					$account = $loggedInUser->display_username;
					$acronymn = $row["Acronymn"];
					$sql_pending = $db->query("SELECT SUM(Amount) AS Pending FROM deposits WHERE `Paid`='0' AND `Account`='$account' AND `Coin`='$acronymn'");
					$row_ = $sql_pending->fetch_assoc();
					$pending = $row_['Pending']; /* $sql_pending */;
                                        $result = @$db->query("SELECT SUM(Amount) as `Amount`  FROM balances WHERE User_ID='$user_id' AND `Wallet_ID` = '$coin'");
                                        if($result == NULL) {
                                                $amount = 0;
                                                $pending = 0;
                                        }else{
						$row_ = $result->fetch_assoc();
                                                $amount = $row_['Amount']; /* $result */;
                                                $account = $loggedInUser->display_username;
                                        }
                                        $flushtxt = '<a href="index.php?page=deposit&id='.$row["Id"].'">'.lang("ACCOUNT_DEPOSIT").'</a>&nbsp;&nbsp;<a href="index.php?page=update_balance&fchk='.$row["Acronymn"].'">'.lang("ACCOUNT_FLUSH").'</a>';
                                        $withdrawtxt = '<a href="index.php?page=withdrawreq&id='.$row["Id"].'">'.lang("ACCOUNT_WITHDRAW").'</a>';
					echo'
					<tr class="'.$color.'">
						<td><a href="index.php?page=trade&market='.$row['Id'].'">'.$row["Name"].'</a></td>
						<td class="value">'.sprintf("%.8f",$amount).'</td>
						<td class="value">'.$pending.'</td>
						<td style="text-align: center;">'.$flushtxt.'</td>
						<td style="text-align: center;">'.$withdrawtxt.'</td>
					</tr>';
			}
		?>
		</table>
		</div>
	</div>
	
	<div class="tabbertab" title="<?php echo lang("ACCOUNT_MENU_OPEN_ORDERS"); ?>">
	<?php include("open_orders_all.php.inc"); ?>
	</div>
</div>
