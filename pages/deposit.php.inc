<?php
if (isDepositDisabled()) {
	echo 'Deposits are currently disabled.';
} else if(!isUserLoggedIn()) { 
	redirect_url('index.php?page=login');
} else {
	$id2 = $db->real_escape_string($_GET["id"]);
	if($id2 == '0') {
		echo "<h3>deposits of this coin are disabled. it is scheduled to be removed.</h3>";
	} else {
		$account  = $loggedInUser->display_username;
		$query    = $db->query("SELECT `Acronymn`, `URI_Scheme` FROM `Wallets` WHERE `Id`='$id2'");
		$row_ = $query->fetch_assoc();
		$acronymn = $row_['Acronymn']; /* $query */;
		$uri_scheme = $row_['URI_Scheme']; /* $query */;
		$userq    = @$db->query("SELECT * FROM `User_Addresses` WHERE `Wallet_ID`='$id2' AND `User_ID`='$id'");
		$row_ = $userq->fetch_assoc();
		$addy     = $row_['Address']; /* $userq */;
		if(isset($_POST["Regenerate"])) {
			$addy = "REGEN";
		}
		if($addy == null) {
			$wallet = new Wallet($id2);
			$addy=$wallet->GetDepositAddress($account);
			@$db->query("INSERT INTO `User_Addresses` VALUES ('$id','$addy','$id2')");
		}
		if($addy == "REGEN") {
			$wallet = new Wallet($id2);
			$addy=$wallet->Client->getnewaddress($account);
			@$db->query("UPDATE `User_Addresses` SET `Address`='$addy' WHERE `Wallet_ID`='$id2' AND `User_ID`='$id'");
		}
		if ($uri_scheme) {
			$addy = "<a href='$uri_scheme:$addy'><img src='https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=$uri_scheme:$addy'>$addy</a>";
		}
		echo "<h3>Your ".$acronymn." deposit address is: </h3>";
		echo "<div>$addy</div>";
		echo '<form action="index.php?page=deposit&id='.$id2.'" method="POST" >
			<input type="submit" name="Regenerate" value="Get New Address" class="blues stdsize"/>
			</form></div>';
	}
}
?>
