<?php
if(!isUserLoggedIn()){
	redirect_url('index.php?page=login');
}
$id = $loggedInUser->user_id;

 /*check to see if user has an api key.*/
 
$api_select = $db->query("SELECT * FROM Api_Keys WHERE `User_ID`='$id'");
if($api_select->num_rows <= 0) {
	$topublic = generateKey($id); //public key
	$toprivate = generateKey($id); //private key
	$pub_check_no_collision = $db->query("SELECT `Public_Key` FROM Api_Keys WHERE `Public_Key` = '$topublic'");
	$priv_check_no_collision = $db->query("SELECT `Authentication_Key` FROM Api_Keys WHERE `Authentication_Key` = '$toprivate'");
	if($pub_check_no_collision->num_rows == 0 ) {
		$api_insert = $db->query("INSERT INTO Api_Keys (`Public_Key`,`Authentication_Key`,`User_ID`) VALUES ('$topublic','$toprivate','$id')");
	}
	redirect_url('index.php?page=api');
}
$row_ = $api_select->fetch_assoc();
$pkey = $row_['Public_Key']; /* $api_select */;
$akey = $row_['Authentication_Key']; /* $api_select */;
?>

<h1>API Information</h1>

<h3>Your Public Key is:</h3>
<input onclick="ClipBoard();" style="width: 70%;" type="text" id="pubkey"  class="field stdsize" readonly value="<?php echo $pkey; ?>" />

<h3>Your Server Key is:</h3>
<input onclick="ClipBoard();" style="width: 70%;" type="text" id="authkey" class="field stdsize" readonly value="<?php echo $akey; ?>" />

<h3>API Reference and Examples</h3>
<a href="ajax.php?do=getapireference">Download Reference(RTF Format)</a>
