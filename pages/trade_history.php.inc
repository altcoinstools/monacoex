<?php
if(!isUserLoggedIn()) 
{
	redirect_url('index.php?page=login');
}
?>
<?php
$id     = $db->real_escape_string($_GET["market"]);
$result = $db->query("SELECT * FROM Wallets WHERE `Id`='$id'");
$row_ = $result->fetch_assoc();
$name   = $db->real_escape_string($row_['Acronymn']); /* $result */;
$type = $name;
$user = $loggedInUser->user_id;
?>
<div class="top" style="text-align: center;">
Your Trade History
</div>
<div class="box">
<table class="page data">
<thead>
<tr>
	<th style="width: 20%;">Date</th>
	<th style="width: 20%;">Type</th>
	<th style="width: 20%;"><?php echo lang("Price"); ?></th>
	<th style="width: 20%;"><?php echo lang("Quantity"); ?>(<?php echo $name;?>)</th>
	<th style="width: 20%;">Total (MONA)</th>
</tr>
</thead>
<tbody>
<?php
$sqlz = $db->query("SELECT * FROM Trade_History WHERE (`Buyer`='$user' OR `Seller`='$user') AND `Market_Id`='$id' ORDER BY `Timestamp` DESC");
$g = 0;
while ($row = $sqlz->fetch_assoc()) {
$g++;
if($g & 1) {
	$color = "lightgray";
} else {
	$color = "darkgray";
}
if($row["Buyer"] == $user)
{
	$info = "Bought";
}
else
{
	$info = "Sold";
}
$time = date("Y-m-d H:i:s", $row["Timestamp"]);
?>
<tr class="<?php echo $color; ?>">
	<td><?php echo $time;?></td>
	<td><?php echo $info;?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Price"]);?></td>
	<td class="value"><?php echo sprintf('%.8f', $row["Quantity"]);?></td>
	<td class="value"><?php echo sprintf('%.8f',$row["Quantity"] * $row["Price"]);?></td>
</tr>
<?php
}
?>
</tbody>
</table>
</div>
